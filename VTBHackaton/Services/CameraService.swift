//
//  CameraService.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 09.10.2020.
//

import AVFoundation

public typealias CaptureHandlerBlock = (CMSampleBuffer?) -> Void

class CameraService: NSObject {
    // MARK: - Private Varaibles
    private var captureSessionQueue: DispatchQueue = DispatchQueue(label: "CaptureSessionQueue")
    private var sampleBufferDelegateQueue: DispatchQueue = DispatchQueue(label: "SampleBufferDelegateQueue")
    
    private lazy var session: AVCaptureSession = {
        let session = AVCaptureSession()
        session.sessionPreset = .photo
        
        return session
    }()
    private var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    private lazy var camera: AVCaptureDevice? = {
        AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back)
    }()
    
    // MARK: - Public Variables
    
    /// Колбэк, который вызывается при появлении нового фрейма видео потока. В блок передается буфер кадра (в формате kCVPixelFormatType_32BGRA)
    var captureHandler: CaptureHandlerBlock?
    
    /// Слой, отображающий видеопоток. Не изменяет свою ориентацию после создания. Используйте его для отображения на UI
    var previewLayer: AVCaptureVideoPreviewLayer? {
        let previewLayer = AVCaptureVideoPreviewLayer(session: session)
        previewLayer.videoGravity = .resizeAspectFill
        
        return previewLayer
    }

    // MARK: - Public Methods
    
    /**
     Конфигурирует сессию захвата видеопотока.
     @param cameraPosition Указывает на позицию камеры.
     @return YES в случае успеха, иначе NO
     */
    func configure() -> Bool {
        guard let camera = self.camera else {
            return false
        }
        
        if camera.isFocusModeSupported(.autoFocus) {
            try? camera.lockForConfiguration()
            camera.focusMode = .continuousAutoFocus
            camera.unlockForConfiguration()
        }
        
        guard let input = try? AVCaptureDeviceInput(device: camera) else {
            return false
        }
        
        guard session.canAddInput(input) else {
            return false
        }
        session.addInput(input)
        
        let output = AVCaptureVideoDataOutput()
        output.videoSettings = [ kCVPixelBufferPixelFormatTypeKey as String : kCVPixelFormatType_32BGRA ]
        output.alwaysDiscardsLateVideoFrames = true
        
        output.setSampleBufferDelegate(self, queue: sampleBufferDelegateQueue)
        
        guard session.canAddOutput(output) else {
            return false
        }
        session.addOutput(output)
        
        let connection = output.connection(with: .video)
        connection?.videoOrientation = .portrait
        
        return true
    }
    
    /**
     Вызывает захват видеопотока с камеры телефона
     @note Для отображения видеопотока на UI используйте свойство previewLayer. Для работы с буфером кадров видеопотока видео потока используйте свойство captureHandler
     @see previewLayer
     @see captureHandler
     */
    func start() {
        guard !session.isRunning else {
            return
        }
        captureSessionQueue.async {
            self.session.startRunning()
        }
    }

    
    /// Останавливает захват видеопотока
    func stop() {
        guard session.isRunning else {
            return
        }
        session.stopRunning()
    }
}

extension CameraService: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        self.captureHandler?(sampleBuffer)
    }
}
