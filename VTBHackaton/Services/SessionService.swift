//
//  SessionService.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import Foundation

final class SessionService {
    public lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        dateFormatter.locale = Locale(identifier: "ru_RU")
        
        return dateFormatter
    }()
    
    // MARK: - Properties
    @UserDefault("familyName")
    private(set) var familyName: String? { didSet { updateState() } }
    
    @UserDefault("firstName")
    private(set) var firstName: String? { didSet { updateState() } }
    
    @UserDefault("middleName")
    private(set) var middleName: String? { didSet { updateState() } }
    
    @UserDefault("birthDateTime")
    private(set) var birthDateTime: String? { didSet { updateState() } }
    
    @UserDefault("birthPlace")
    private(set) var birthPlace: String? { didSet { updateState() } }
    
    @UserDefault("nationality")
    private(set) var nationality: String? { didSet { updateState() } }
    
    @UserDefault("gender")
    private(set) var gender: String? { didSet { updateState() } }
    
    @UserDefault("clientIncome")
    private(set) var clientIncome: Int? { didSet { updateState() } }
    
    private var isRulesAccepted: Bool = false { didSet { updateState() } }
    
    public var isComplete: Bool = false {
        didSet {
            stateUpdated?(isComplete)
        }
    }
    
    public var stateUpdated: ((Bool) -> ())?
    
    private func updateState() {
        let isComplete = familyName != nil &&
            firstName != nil &&
            birthDateTime != nil &&
            birthPlace != nil &&
            nationality != nil &&
            gender != nil &&
            clientIncome != nil &&
            isRulesAccepted
        
        if self.isComplete != isComplete {
            self.isComplete = isComplete
        }
    }
    
    init() {
        updateState()
    }
}

extension SessionService {
    func save(name: String) -> Bool {
        var nameComponents = name.components(separatedBy: " ")
        guard case 2...3 = nameComponents.count else {
            return false
        }
        
        self.familyName = nameComponents.removeFirst()
        self.firstName = nameComponents.removeFirst()
        if !nameComponents.isEmpty {
            self.middleName = nameComponents.removeFirst()
        } else {
            self.middleName = nil
        }
        
        return true
    }
    
    func save(birthDate: String) -> Bool {
        self.birthDateTime = birthDate
        
        return true
    }
    
    func save(birthPlace: String) -> Bool {
        self.birthPlace = birthPlace
        
        return true
    }
    
    func save(nationality: String) -> Bool {
        self.nationality = nationality
        
        return true
    }
    
    func save(gender: String) -> Bool {
        switch gender {
        case "Мужчина":
            self.gender = Gender.male.rawValue
            return true
        case "Женщина":
            self.gender = Gender.female.rawValue
            return true
        case "Неизвестно":
            self.gender = Gender.unknown.rawValue
            return true
        default:
            return false
        }
    }
    
    func save(clientIncome: String) -> Bool {
        guard let clientIncome = Int(clientIncome) else {
            return false
        }
        self.clientIncome = clientIncome
        
        return true
    }
    
    func set(rulesAccepted: Bool) {
        self.isRulesAccepted = rulesAccepted
    }
}
