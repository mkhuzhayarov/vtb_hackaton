//
//  AuthService.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import Foundation

import PromiseKit

protocol AuthServiceProtocol {
}

final class AuthService {
    private let authAPI: AuthHackatonAPIProtocol
    
    init(authAPI: AuthHackatonAPIProtocol) {
        self.authAPI = authAPI
    }
}

// MARK: - Public
extension AuthService: AuthServiceProtocol {
    
}
