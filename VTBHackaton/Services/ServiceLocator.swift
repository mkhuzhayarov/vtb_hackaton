//
//  ServiceLocator.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 10.10.2020.
//

import Foundation

class ServiceLocator {
    // MARK: - Services
    let apiService: HackatonAPI = HackatonAPI(credentials: .shared)
    let imageService: ImageAPI = ImageAPI()
    let cameraService: CameraService = CameraService()
    let sessionService: SessionService = SessionService()
    
    // MARK: - Licecycle
    private static var uniqueInstance: ServiceLocator?
    private init() {}

    static func shared() -> ServiceLocator {
        if uniqueInstance == nil {
            uniqueInstance = ServiceLocator()
        }
        return uniqueInstance!
    }
}
