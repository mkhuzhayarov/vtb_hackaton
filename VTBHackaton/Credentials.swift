//
//  Credentials.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import Foundation

final class Credentials {
    static let shared = Credentials()
    
    private let userDefaults: UserDefaults
    private let credentialStorage: URLCredentialStorage
    
    init(userDefaults: UserDefaults = .standard, credentialStorage: URLCredentialStorage = .shared) {
        self.userDefaults = userDefaults
        self.credentialStorage = credentialStorage
    }
}

// MARK: - Private
extension Credentials {
    private func credential(for namespace: Namespace) -> URLCredential? {
        credentialStorage.defaultCredential(for: namespace.protectionSpace)
    }
    
    private func defaultsKey(for namespace: Namespace) -> String {
        switch namespace {
        case .hackaton:
            return "com.hackaton.defaults.hackaton.userInfo.key"
        }
    }
}

// MARK: - Public
extension Credentials {
    func user(for namespace: Namespace) -> String? {
        credential(for: namespace)?.user
    }
    
    func token(for namespace: Namespace) -> String? {
        credential(for: namespace)?.password
    }
    
    func userInfo(for namespace: Namespace) -> [String: Any]? {
        let key = defaultsKey(for: namespace)
        return userDefaults.dictionary(forKey: key)
    }
    
    func set(user: String, token: String, for namespace: Namespace) {
        set(user: user, token: token, userInfo: nil, for: namespace)
    }
    
    func set(user: String, token: String, userInfo: [String: Any]?, for namespace: Namespace) {
        if let existingCredential = credential(for: namespace) {
            credentialStorage.remove(
                existingCredential,
                for: namespace.protectionSpace
            )
        }
        
        let credential = URLCredential(
            user: user,
            password: token,
            persistence: .permanent
        )
        
        self.credentialStorage.setDefaultCredential(
            credential,
            for: namespace.protectionSpace
        )
        
        let key = defaultsKey(for: namespace)
        userDefaults.setValue(userInfo, forKey: key)
    }
    
    func clear(namespace: Namespace) {
        guard let credential = credential(for: namespace) else {
            return
        }
        
        credentialStorage.remove(
            credential,
            for: namespace.protectionSpace
        )
        
        let key = defaultsKey(for: namespace)
        userDefaults.setValue(nil, forKey: key)
    }
}
