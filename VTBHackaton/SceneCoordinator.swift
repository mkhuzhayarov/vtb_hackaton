//
//  SceneCoordinator.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 10.10.2020.
//

import UIKit

protocol SceneCoordinatorProtocol {
    func start()
}

class SceneCoordinator {
    weak var window: UIWindow!
    var context: UINavigationController = UINavigationController()
    
    weak var scannerViewController: ScannerViewController?
    weak var loanViewController: LoanViewController?
    
    var carLoan: CarLoan?
    
    init(window: UIWindow) {
        self.window = window
    }
}

extension SceneCoordinator: SceneCoordinatorProtocol {
    func start() {
        let rootViewController = SceneAssembly.onboardingView()
        
        self.window.rootViewController = rootViewController
        self.window.makeKeyAndVisible()
        
        self.showScannerView()
    }
    
    func showScannerView() {
        let scannerViewController = SceneAssembly.scannerView(with: self)
        
        context.setViewControllers([scannerViewController], animated: false)
        context.loadViewIfNeeded()
        
        self.scannerViewController = scannerViewController
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DispatchTimeInterval.milliseconds(500)) {
            self.window.rootViewController = self.context
        }
    }
    
    func showSuggestView(with carModel: CarModel) {
        let suggestViewController = SceneAssembly.suggestView(with: self)
        suggestViewController.configure(withCarModel: carModel)
        
        scannerViewController?.addPanModal(viewController: suggestViewController)
    }
    
    func showLoanView(with carModel: CarModel, calculation: CalculationRequest?) {
        let loanViewController = SceneAssembly.loanView(with: self)
        loanViewController.configure(withCarModel: carModel, calculation: calculation)
        
        context.pushViewController(loanViewController, animated: true)
        
        self.loanViewController = loanViewController
    }
    
    func showLoginView() {
        let loginViewController = SceneAssembly.loginView(with: self)
        
        context.pushViewController(loginViewController, animated: true)
    }
    
    func showOTPView(with phone: Phone) {
        let otpViewController = SceneAssembly.otpView(with: self)
        otpViewController.configure(withPhone: phone)
        
        context.pushViewController(otpViewController, animated: true)
    }
    
    func showFormView(with carLoan: CarLoan) {
        let formViewController = SceneAssembly.formView(with: self)
        formViewController.configure(withCarLoan: carLoan)
        
        context.pushViewController(formViewController, animated: true)
    }
    
    func showGraphView(with payments: [Payment]) {
        let graphViewController = SceneAssembly.graphView()
        graphViewController.configure(withPayments: payments)
        
        context.present(UINavigationController(rootViewController: graphViewController), animated: true, completion: nil)
    }
    
    func showDecisionView(with carLoanDecision: CarLoanResponse) {
        let decisionViewController = SceneAssembly.decisionView(with: self)
        decisionViewController.configure(withCarLoanDecision: carLoanDecision)
        
        context.pushViewController(decisionViewController, animated: true)
    }
}

extension SceneCoordinator: ScannerControllerDelegate {
    func scannerController(_ scanner: ScannerViewController, didFinishScanningWithCar carModel: CarModel) {
        self.showSuggestView(with: carModel)
    }
}

extension SceneCoordinator: SuggestControllerDelegate {
    func suggestControllerWillDisappear(_ viewController: SuggestViewController) {
        scannerViewController?.startCamera()
    }
    
    func suggestControllerDidDisappear(_ viewController: SuggestViewController) {
        
    }
    
    func suggestController(_ viewController: SuggestViewController, didSelectRecommendedCar carModel: CarModel) {
        viewController.dismiss(animated: true) {
            self.scannerViewController?.stopCamera()
            self.showSuggestView(with: carModel)
        }
    }
    
    func suggestController(_ viewController: SuggestViewController, didSelectCar carModel: CarModel, withCalculation calculation: (CalculationRequest, CalculationResponse)?) {
        viewController.dismiss(animated: true)
        if let (calculationRequest, calculationResponse) = calculation {
            let currentDate = Date()
            let interestRate = calculationResponse.result.contractRate
            let requestedAmount = calculationResponse.result.loanAmount
            let requestedTerm = calculationResponse.result.term
            let tradeMark = carModel.brand.alias
            let vehicleCost = carModel.minPrice
            
            let carLoan = CarLoan(
                dateTime: currentDate,
                interestRate: interestRate,
                requestedAmount: requestedAmount,
                requestedTerm: requestedTerm,
                tradeMark: tradeMark,
                vehicleCost: vehicleCost,
                customerParty: nil,
                comment: nil
            )
            
            self.carLoan = carLoan
            showLoginView()
        } else {
            showLoanView(with: carModel, calculation: nil)
        }
    }
    
    func suggestControllerWouldBeDismissed(_ viewController: SuggestViewController) {
        viewController.dismiss(animated: true)
    }
}

extension SceneCoordinator: LoanControllerDelegate {
    func loanController(_ loan: LoanViewController, didRequestPaymentsGraph payments: [Payment]) {
        self.showGraphView(with: payments)
    }
    
    func loanController(_ loan: LoanViewController, didFinishWorkWithCarLoan carLoan: CarLoan) {
        self.carLoan = carLoan
        self.showLoginView()
    }
}

extension SceneCoordinator: LoginControllerDelegate {
    func loginController(_ login: LoginViewController, didFinishWorkWithPhone phone: Phone) {
        self.showOTPView(with: phone)
    }
}

extension SceneCoordinator: OTPControllerDelegate {
    func otpController(_ otp: OTPViewController, didFinishWorkWithInfo info: Any) {
        guard let carLoan = carLoan else {
            return
        }
        if let loanViewController = loanViewController, context.viewControllers.contains(loanViewController) {
            context.popToViewController(loanViewController, animated: false)
        }
        self.showFormView(with: carLoan)
    }
}

extension SceneCoordinator: FormControllerDelegate {
    func formController(_ form: FormViewController, didFinishWorkWithCarLoanResponse carLoan: CarLoanResponse) {
        self.showDecisionView(with: carLoan)
    }
}

extension SceneCoordinator: DecisionControllerDelegate {
    
}
