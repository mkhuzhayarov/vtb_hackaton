//
//  ImageAPI.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 10.10.2020.
//

import Foundation

import PromiseKit

protocol ImageAPIProtocol {
    func loadImage(with path: String) -> Promise<Data>
}

final class ImageAPI {
    private let session: URLSession
    
    init(session: URLSession = .shared) {
        self.session = session
    }
}

extension ImageAPI: ImageAPIProtocol {
    func loadImage(with path: String) -> Promise<Data> {
        guard
            let percentEncodingPath = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
            let components = URLComponents(string: percentEncodingPath),
            let url = components.url
        else {
            return Promise(error: ImageError.invalidURL)
        }
        
        let request = URLRequest(url: url)
        
        return firstly {
            session.dataTask(.promise, with: request)
        }.validate(statusCode: .ok).map { data, response in
            return data
        }
    }
}
