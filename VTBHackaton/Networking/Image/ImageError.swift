//
//  ImageError.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 10.10.2020.
//

import Foundation

enum ImageError: Error {
    case invalidURL
}
