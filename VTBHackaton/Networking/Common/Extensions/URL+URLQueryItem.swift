//
//  URL+URLQueryItem.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import Foundation

extension URL {
    func appending(_ queryItem: URLQueryItem) -> URL {
        guard var urlComponents = URLComponents(url: self, resolvingAgainstBaseURL: true) else {
            return self
        }
        urlComponents.queryItems = (urlComponents.queryItems ?? []) + [queryItem]
        return urlComponents.url!
    }
}
