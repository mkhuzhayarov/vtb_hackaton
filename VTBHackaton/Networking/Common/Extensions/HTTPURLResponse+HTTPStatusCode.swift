//
//  HTTPURLResponse+HTTPStatusCode.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 08.10.2020.
//

import Foundation

extension HTTPURLResponse {
    var httpStatusCode: HTTPStatusCode? {
        HTTPStatusCode(rawValue: statusCode)
    }
    
    func validate(statusCode: HTTPStatusCode) throws {
        guard let status = self.httpStatusCode, status != statusCode else {
            return
        }
        throw status
    }

    func validate(responseType: HTTPStatusCode.ResponseType) throws {
        guard let status = self.httpStatusCode, status.responseType != responseType else {
            return
        }
        throw status
    }
}
