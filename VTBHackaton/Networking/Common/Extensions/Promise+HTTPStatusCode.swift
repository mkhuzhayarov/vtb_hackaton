//
//  Promise+HTTPStatusCode.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import PromiseKit

import Foundation

extension Promise where T == (data: Data, response: URLResponse) {
    func validate(statusCode: HTTPStatusCode) -> Promise<T> {
        return map {
            if let response = $0.response as? HTTPURLResponse {
                try response.validate(statusCode: statusCode)
            }
            return $0
        }
    }
    
    func validate(responseType: HTTPStatusCode.ResponseType) -> Promise<T> {
        return map {
            if let response = $0.response as? HTTPURLResponse {
                try response.validate(responseType: responseType)
            }
            return $0
        }
    }
}
