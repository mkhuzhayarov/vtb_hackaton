//
//  HackatonAPI.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 08.10.2020.
//

import Foundation

import PromiseKit
import PMKFoundation

private enum Const {
    static let namespace: Namespace = .hackaton
    static let path: String = "api"
    
    static let baseUrl: URL = {
        var components = URLComponents()
        components.scheme = namespace.scheme
        components.host = namespace.host
        return components.url!.appendingPathComponent(path)
    }()
    
    static let additionalHeaders: [String: Any] = [
        "Content-Type" : "application/json",
        "Accept" : "application/json"
    ]
}

protocol AuthHackatonAPIProtocol {
    func requestCode(with phone: Phone) -> Promise<()>
    
    func auth(with phoneCode: PhoneCode) -> Promise<(profile: Profile, user: User)>
}

protocol ProfileHackatonAPIProtocol {
    func profile(withId id: Int) -> Promise<Profile>
    
    func updateProfile(withId id: Int, new profile: Profile) -> Promise<Profile>
}

protocol CarLoanHackatonAPIProtocol {
    func loanCar(_ loan: CarLoan) -> Promise<CarLoanResponse>
}

protocol CarRecognitionHackatonAPIProtocol {
    func recognizeCar(on image: CarImage) -> Promise<CarResponse>
}

protocol MarketplaceHackatonAPIProtocol {
    func marketplace(ofBrandName brandName: String, modelName: String?) -> Promise<[CarModel]>
}

protocol CalculatorHackatonAPIProtocol {
    func calculate(_ request: CalculationRequest) -> Promise<CalculationResponse>
    
    func paymentsGraph(_ request: GraphRequest) -> Promise<GraphResponse>
    
    func settings(forBrandName brandName: String, language: Language) -> Promise<CalculatorSettings>
}

final class HackatonAPI {
    private let session: URLSession
    private let credentials: Credentials
    
    private let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter
    }()
    
    private let dateTimeFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter
    }()
    
    init(session: URLSession, credentials: Credentials = .shared) {
        self.session = session
        self.credentials = credentials
    }
    
    init(credentials: Credentials) {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Const.additionalHeaders
        
        self.session = URLSession(configuration: configuration)
        self.credentials = credentials
    }
}

extension HackatonAPI: AuthHackatonAPIProtocol {
    func requestCode(with phone: Phone) -> Promise<()> {
        // Build URL
        let url: URL = Const.baseUrl.appendingPathComponent("requestOTP")
        
        // Build Request
        let encoder = JSONEncoder()
        let requestBody = try! encoder.encode(phone)
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = requestBody
        
        return firstly {
            session.dataTask(.promise, with: request)
        }.validate(statusCode: .ok).map { _ in }
    }
    
    func auth(with phoneCode: PhoneCode) -> Promise<(profile: Profile, user: User)> {
        // Build URL
        let url: URL = Const.baseUrl.appendingPathComponent("auth")
        
        // Build Request
        let encoder = JSONEncoder()
        let requestBody = try! encoder.encode(phoneCode)
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = requestBody
        
        // Send Request and Handle Response
        return firstly {
            session.dataTask(.promise, with: request)
        }.validate(statusCode: .ok).map {
            return $0.data
        }.map { data -> AuthResponse in
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .formatted(self.dateFormatter)
            
            return try decoder.decode(AuthResponse.self, from: data)
        }.map { response in
            self.credentials.set(
                user: response.user.userName,
                token: response.token,
                for: .hackaton
            )
            
            return (response.profile, response.user)
        }
    }
}
    
// MARK: - Public
extension HackatonAPI: ProfileHackatonAPIProtocol {
    func profile(withId id: Int) -> Promise<Profile> {
        // Check for Credentials
        guard let token = credentials.token(for: .hackaton) else {
            return Promise(error: HackatonError.credentialsNotFound)
        }
        
        // Build URL
        let url: URL = Const.baseUrl
            .appendingPathComponent("profile")
            .appendingPathComponent(String(id))
        
        // Build Request
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("Token \(token)", forHTTPHeaderField: "Authorization")
        
        // Send Request and Handle Response
        return firstly {
            session.dataTask(.promise, with: request)
        }.validate(statusCode: .ok).map {
            return $0.data
        }.map { data -> Profile in
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .formatted(self.dateFormatter)
            
            return try decoder.decode(Profile.self, from: data)
        }
    }
    
    func updateProfile(withId id: Int, new profile: Profile) -> Promise<Profile> {
        // Check for Credentials
        guard let token = credentials.token(for: .hackaton) else {
            return Promise(error: HackatonError.credentialsNotFound)
        }
        
        // Build URL
        let url: URL = Const.baseUrl
            .appendingPathComponent("profile")
            .appendingPathComponent(String(id))
        
        // Build Request
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .formatted(self.dateFormatter)
        
        let requestBody = try! encoder.encode(profile)
        
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        request.httpBody = requestBody
        request.setValue("Token \(token)", forHTTPHeaderField: "Authorization")
        
        // Send Request and Handle Response
        return firstly {
            session.dataTask(.promise, with: request)
        }.validate(statusCode: .ok).map {
            return $0.data
        }.map { data -> Profile in
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .formatted(self.dateFormatter)
            
            return try decoder.decode(Profile.self, from: data)
        }
    }
}

extension HackatonAPI: CarLoanHackatonAPIProtocol {
    func loanCar(_ loan: CarLoan) -> Promise<CarLoanResponse> {
        // Check for Credentials
        guard let token = credentials.token(for: .hackaton) else {
            return Promise(error: HackatonError.credentialsNotFound)
        }
        
        // Build URL
        let url: URL = Const.baseUrl.appendingPathComponent("carloan")
        
        // Build Request
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .formatted(self.dateTimeFormatter)
        
        let requestBody = try! encoder.encode(loan)
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = requestBody
        request.setValue("Token \(token)", forHTTPHeaderField: "Authorization")
        
        
        // Send Request and Handle Response
        return firstly {
            session.dataTask(.promise, with: request)
        }.validate(statusCode: .ok).map {
            return $0.data
        }.map { data -> CarLoanResponse in
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .formatted(self.dateTimeFormatter)
            
            return try decoder.decode(CarLoanResponse.self, from: data)
        }
    }
}

extension HackatonAPI: CarRecognitionHackatonAPIProtocol {
    func recognizeCar(on image: CarImage) -> Promise<CarResponse> {
        // Build URL
        let url: URL = Const.baseUrl.appendingPathComponent("car-recognize")
        
        // Build Request
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .formatted(self.dateTimeFormatter)
        let requestBody = try! encoder.encode(image)
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = requestBody
        
        // Send Request and Handle Response
        return firstly {
            session.dataTask(.promise, with: request)
        }.validate(statusCode: .ok).map {
            return $0.data
        }.map { data -> CarResponse in
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .formatted(self.dateTimeFormatter)
            
            return try decoder.decode(CarResponse.self, from: data)
        }
    }
}
    
extension HackatonAPI: MarketplaceHackatonAPIProtocol {
    func marketplace(ofBrandName brandName: String, modelName: String?) -> Promise<[CarModel]> {
        // Build URL
        let url: URL = Const.baseUrl
            .appendingPathComponent("marketplace")
            .appending(URLQueryItem(name: "brand", value: brandName))
            .appending(URLQueryItem(name: "model", value: modelName))
        
        // Build Request
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        // Send Request and Handle Response
        return firstly {
            session.dataTask(.promise, with: request)
        }.validate(statusCode: .ok).map {
            return $0.data
        }.map { data -> [CarModel] in
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .formatted(self.dateTimeFormatter)
            
            return try decoder.decode([CarModel].self, from: data)
        }
    }
}

extension HackatonAPI: CalculatorHackatonAPIProtocol {
    func calculate(_ request: CalculationRequest) -> Promise<CalculationResponse> {
        // Build URL
        let url: URL = Const.baseUrl
            .appendingPathComponent("calculator")
            .appendingPathComponent("calculate")
        
        // Build Request
        let encoder = JSONEncoder()
        let requestBody = try! encoder.encode(request)
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = requestBody
        
        // Send Request and Handle Response
        return firstly {
            session.dataTask(.promise, with: request)
        }.validate(statusCode: .ok).map {
            return $0.data
        }.map { data -> CalculationResponse in
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .formatted(self.dateTimeFormatter)
            
            return try decoder.decode(CalculationResponse.self, from: data)
        }
    }
    
    func paymentsGraph(_ request: GraphRequest) -> Promise<GraphResponse> {
        // Build URL
        let url: URL = Const.baseUrl
            .appendingPathComponent("calculator")
            .appendingPathComponent("payments-graph")
        
        // Build Request
        let encoder = JSONEncoder()
        let requestBody = try! encoder.encode(request)
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = requestBody
        
        // Send Request and Handle Response
        return firstly {
            session.dataTask(.promise, with: request)
        }.validate(statusCode: .ok).map {
            return $0.data
        }.map { data -> GraphResponse in
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .formatted(self.dateTimeFormatter)
            
            return try decoder.decode(GraphResponse.self, from: data)
        }
    }
    
    func settings(forBrandName brandName: String, language: Language) -> Promise<CalculatorSettings> {
        // Build URL
        let url: URL = Const.baseUrl
            .appendingPathComponent("calculator")
            .appendingPathComponent("settings")
            .appending(URLQueryItem(name: "name", value: brandName))
            .appending(URLQueryItem(name: "language", value: language.rawValue))
        
        // Build Request
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        // Send Request and Handle Response
        return firstly {
            session.dataTask(.promise, with: request)
        }.validate(statusCode: .ok).map {
            return $0.data
        }.map { data -> CalculatorSettings in
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .formatted(self.dateTimeFormatter)
            
            return try decoder.decode(CalculatorSettings.self, from: data)
        }
    }
}
