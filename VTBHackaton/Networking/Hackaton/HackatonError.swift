//
//  HackatonError.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import Foundation

enum HackatonError: Error {
    case credentialsNotFound
}
