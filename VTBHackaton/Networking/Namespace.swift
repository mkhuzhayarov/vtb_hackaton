//
//  Namespace.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import Foundation

enum Namespace {
    case hackaton
    
    var scheme: String {
        NSURLProtectionSpaceHTTPS
    }
    
    var host: String {
        switch self {
        case .hackaton:
            return "sfera.space"
        }
    }
    
    var protectionSpace: URLProtectionSpace {
        switch self {
        case .hackaton:
            return URLProtectionSpace(
                host: host,
                port: 443,
                protocol: scheme,
                realm: "",
                authenticationMethod: NSURLAuthenticationMethodDefault
            )
        }
        
    }
}
