//
//  SceneAssembly.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 09.10.2020.
//

import UIKit

class SceneAssembly {    
    class func onboardingView() -> UIViewController {
        return UIStoryboard(name: "OnboardingView", bundle: nil).instantiateInitialViewController()!
    }
    
    class func scannerView(with delegate: ScannerControllerDelegate) -> ScannerViewController {
        let scannerView = UIStoryboard(name: "ScannerView", bundle: nil).instantiateInitialViewController() as! ScannerViewController
        scannerView.apiService = ServiceLocator.shared().apiService
        scannerView.cameraService = ServiceLocator.shared().cameraService
        
        scannerView.delegate = delegate
        
        return scannerView
    }
    
    class func suggestView(with delegate: SuggestControllerDelegate) -> SuggestViewController {
        let suggestView = UIStoryboard(name: "SuggestView", bundle: nil).instantiateInitialViewController() as! SuggestViewController
        suggestView.apiService = ServiceLocator.shared().apiService
        suggestView.imageService = ServiceLocator.shared().imageService
        suggestView.delegate = delegate
        
        return suggestView
    }
    
    class func loanView(with delegate: LoanControllerDelegate) -> LoanViewController {
        let loanView = UIStoryboard(name: "LoanView", bundle: nil).instantiateInitialViewController() as! LoanViewController
        loanView.apiService = ServiceLocator.shared().apiService
        loanView.imageService = ServiceLocator.shared().imageService
        
        loanView.delegate = delegate
        
        return loanView
    }
    
    class func cascoView(with delegate: CascoControllerDelegate) -> CascoViewController {
        let cascoView = UIStoryboard(name: "CascoView", bundle: nil).instantiateInitialViewController() as! CascoViewController
        
        cascoView.delegate = delegate
        
        return cascoView
    }
    
    class func graphView() -> GraphViewController {
        return UIStoryboard(name: "GraphView", bundle: nil).instantiateInitialViewController() as! GraphViewController
    }
    
    class func loginView(with delegate: LoginControllerDelegate) -> LoginViewController {
        let loginView = UIStoryboard(name: "LoginView", bundle: nil).instantiateInitialViewController() as! LoginViewController
        
        loginView.delegate = delegate
        
        return loginView
    }
    
    class func otpView(with delegate: OTPControllerDelegate) -> OTPViewController {
        let otpView = UIStoryboard(name: "OTPView", bundle: nil).instantiateInitialViewController() as! OTPViewController
        otpView.sessionService = ServiceLocator.shared().sessionService
        otpView.apiService = ServiceLocator.shared().apiService
        
        otpView.delegate = delegate
        
        return otpView
    }
    
    class func formView(with delegate: FormControllerDelegate) -> FormViewController {
        let formView = UIStoryboard(name: "FormView", bundle: nil).instantiateInitialViewController() as! FormViewController
        formView.sessionService = ServiceLocator.shared().sessionService
        formView.apiService = ServiceLocator.shared().apiService
        
        formView.delegate = delegate
        
        return formView
    }
    
    class func decisionView(with delegate: DecisionControllerDelegate) -> DecisionViewController {
        let decisionView = UIStoryboard(name: "DecisionView", bundle: nil).instantiateInitialViewController() as! DecisionViewController
        
        decisionView.delegate = delegate
        
        return decisionView
    }
}
