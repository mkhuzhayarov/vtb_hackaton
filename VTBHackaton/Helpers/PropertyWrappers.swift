//
//  PropertyWrappers.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 11.10.2020.
//

import Foundation

@propertyWrapper
struct UserDefault<T> {
    let key: String

    init(_ key: String) {
        self.key = key
    }

    var wrappedValue: T? {
        get {
            return UserDefaults.standard.object(forKey: key) as? T
        }
        set {
            UserDefaults.standard.set(newValue, forKey: key)
        }
    }
}
