//
//  Debounce.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 10.10.2020.
//

import Foundation

typealias Debounce = () -> Void

func debounce(interval: Int, queue: DispatchQueue, action: @escaping Debounce) -> Debounce {
    var lastFireTime = DispatchTime.now()
    let dispatchDelay = DispatchTimeInterval.milliseconds(interval)

    return {
        lastFireTime = DispatchTime.now()
        let dispatchTime: DispatchTime = DispatchTime.now() + dispatchDelay

        queue.asyncAfter(deadline: dispatchTime) {
            let when: DispatchTime = lastFireTime + dispatchDelay
            let now = DispatchTime.now()
            if now.rawValue >= when.rawValue {
                action()
            }
        }
    }
}
