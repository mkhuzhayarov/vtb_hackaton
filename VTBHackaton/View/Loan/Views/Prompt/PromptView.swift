//
//  PromptView.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 10.10.2020.
//

import UIKit

final class PromptView: UIView {
    // MARK: - Private Variables
    private let promptLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12.0, weight: .regular)
        label.textColor = .label
        label.numberOfLines = 0
        return label
    }()
    
    private let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        
        return view
    }()
    
    private var pivotLayer: CAShapeLayer?
    
    // MARK: - Public Variables
    var pivotX: CGFloat? {
        didSet {
            pivotLayer?.removeFromSuperlayer()
            pivotLayer = nil
            
            guard let pivotX = pivotX else {
                return
            }
            
            let path = CGMutablePath()
            path.move(to: CGPoint(x: pivotX, y: 0.0))
            path.addLine(to: CGPoint(x: pivotX + 8.0, y: 8.0))
            path.addLine(to: CGPoint(x: pivotX - 8.0, y: 8.0))
            path.addLine(to: CGPoint(x: pivotX, y: 0.0))
            
            let newPivotLayer = CAShapeLayer()
            newPivotLayer.path = path
            newPivotLayer.fillColor = UIColor.lightGray.withAlphaComponent(0.2).cgColor
            
            layer.insertSublayer(newPivotLayer, at: 0)
            
            self.pivotLayer = newPivotLayer
        }
    }
    
    var text: String? {
        get {
            promptLabel.text
        }
        set {
            promptLabel.text = newValue
        }
    }
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
}

// MARK: - Private Functions
extension PromptView {
    private func setupUI() {
        backgroundColor = UIColor.white
        addSubview(containerView)
        containerView.addSubview(promptLabel)
        setupConstraints()
    }
    
    private func setupConstraints() {
        containerView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(8.0)
            make.leading.trailing.bottom.equalToSuperview()
        }
        
        promptLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(16.0)
            make.top.bottom.equalToSuperview().inset(20.0)
        }
    }
}
