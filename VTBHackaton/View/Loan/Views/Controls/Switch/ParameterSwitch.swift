//
//  ParameterSwitch.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 10.10.2020.
//

import UIKit

import SnapKit

class ParameterSwitch: UIView {
    // MARK: - Private Variables
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.spacing = 12.0
        return stackView
    }()
    
    private let titleLabel: UILabel = {
        let label: UILabel = UILabel()
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 16.0, weight: .medium)
        label.textColor = .label
        label.numberOfLines = 2
        
        return label
    }()
    
    private lazy var switchView: UISwitch = {
        let switcher = UISwitch()
        switcher.onTintColor = UIColor(named: "AccentColor")
        switcher.addTarget(self, action: #selector(updateSwitch(_:)), for: .valueChanged)
        return switcher
    }()
    
    private let emptyView: UIView = UIView()
    
    // MARK: - Private Functions
    @objc func updateSwitch(_ sender: UISwitch) {
        // TODO: Сделать удобное округление
        switchOn = sender.isOn
        switchOnUpdated?(switchOn)
    }
    
    // MARK: - Public
    var title: String? {
        set {
            titleLabel.text = newValue
        }
        get {
            titleLabel.text
        }
    }
    
    var switchOn: Bool {
        set {
            switchView.isOn = newValue
        }
        get {
            switchView.isOn
        }
    }
    
    var detailView: UIView? {
        didSet {
            if let oldValue = oldValue {
                stackView.removeArrangedSubview(oldValue)
                stackView.removeArrangedSubview(emptyView)
                
                oldValue.removeFromSuperview()
                emptyView.removeFromSuperview()
                
                titleLabel.numberOfLines = 2
            }
            
            if let detailView = detailView {
                stackView.insertArrangedSubview(detailView, at: 1)
                stackView.insertArrangedSubview(emptyView, at: 2)
                
                titleLabel.numberOfLines = 1
            }
        }
    }
    
    var switchOnUpdated: ((Bool) -> Void)?
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
}

// MARK: - Setup UI
extension ParameterSwitch {
    func setupUI() {
        addSubview(stackView)
        
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(switchView)
        
        stackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        titleLabel.setContentHuggingPriority(.init(249), for: .horizontal)
        titleLabel.setContentHuggingPriority(.init(251), for: .vertical)
        titleLabel.setContentCompressionResistancePriority(.init(249), for: .horizontal)
        
        emptyView.setContentHuggingPriority(.init(248), for: .horizontal)
        emptyView.setContentCompressionResistancePriority(.init(248), for: .horizontal)
    }
}
