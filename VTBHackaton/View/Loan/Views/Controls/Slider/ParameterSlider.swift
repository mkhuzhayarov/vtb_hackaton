//
//  ParameterSlider.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 10.10.2020.
//

import UIKit

import SnapKit

class ParameterSlider: UIView {
    // MARK: - Private Variables
    private let titleLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = .systemFont(ofSize: 16.0, weight: .regular)
        label.textColor = .secondaryLabel
        
        return label
    }()
    
    private let subtitleLabel: UILabel = {
        let label: UILabel = UILabel()
        label.textColor = .secondaryLabel
        label.font = .systemFont(ofSize: 16.0, weight: .regular)
        label.textAlignment = .right
        
        return label
    }()
    
    private let valueLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = .systemFont(ofSize: 16.0, weight: .medium)
        label.textColor = .label
        
        return label
    }()
    
    private lazy var slider: UISlider = {
        let slider = UISlider()
        slider.setThumbImage(UIImage(named: "slider-thumb-image"), for: .normal)
        slider.addTarget(self, action: #selector(updateValue(_:)), for: .valueChanged)
        
        return slider
    }()
    
    private lazy var priceFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = 0
        formatter.locale = Locale(identifier: "ru_RU")
        
        return formatter
    }()
    
    // MARK: - Private Functions
    @objc func updateValue(_ sender: UISlider) {
        let value = Int(sender.value)
        
        valueLabel.text = "\(priceFormatter.string(from: NSNumber(value: value)) ?? "")"
        
        currentValue = Int(sender.value)
        currentValueUpdated?(currentValue)
    }
    
    
    // MARK: - Public
    var title: String? {
        set { titleLabel.text = newValue }
        get { titleLabel.text }
    }
    var formatStyle: NumberFormatter.Style {
        set { priceFormatter.numberStyle = newValue }
        get { priceFormatter.numberStyle }
    }
    var subtitle: String? {
        set {
            if let newValue = newValue, let intTitle = Int(newValue) {
                subtitleLabel.text = "До \(priceFormatter.string(from: NSNumber(value: intTitle)) ?? "")"
            } else {
                subtitleLabel.text = "До \(newValue ?? "")"
            }
        }
        get { subtitleLabel.text }
    }
    var minValue: Int {
        set { slider.minimumValue = Float(newValue) }
        get { Int(slider.minimumValue) }
    }
    var maxValue: Int {
        set { slider.maximumValue = Float(newValue) }
        get { Int(slider.maximumValue) }
    }
    var currentValue: Int {
        set {
            var value = newValue
            if value < minValue { value = minValue }
            if value > maxValue { value = maxValue }
            
            slider.value = Float(value)
            
            if formatStyle == .none {
                let yearsString: String
                switch value {
                    case 1: yearsString = "год"
                    case 2...4: yearsString = "года"
                    case 5...7: yearsString = "лет"
                    default: yearsString = ""
                }
                valueLabel.text = " \(value) \(yearsString)"
            } else {
                valueLabel.text = "\(priceFormatter.string(from: NSNumber(value: newValue)) ?? "")"
            }
        }
        get { Int(slider.value) }
    }
    
    var currentValueUpdated: ((Int) -> Void)?
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
}

// MARK: - Setup UI
extension ParameterSlider {
    func setupUI() {
        let titlesStackView = UIStackView()
        titlesStackView.axis = .horizontal
        titlesStackView.addArrangedSubview(titleLabel)
        titlesStackView.addArrangedSubview(subtitleLabel)
        
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.addArrangedSubview(titlesStackView)
        stackView.addArrangedSubview(valueLabel)
        stackView.addArrangedSubview(slider)
        
        stackView.setCustomSpacing(6.0, after: titlesStackView)
        stackView.setCustomSpacing(8.0, after: valueLabel)
        
        addSubview(stackView)
        
        stackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        titleLabel.setContentHuggingPriority(.defaultHigh, for: .horizontal)
    }
}
