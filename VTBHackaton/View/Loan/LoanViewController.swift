//
//  LoanViewController.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 09.10.2020.
//

import UIKit

import PromiseKit

class LoanViewController: UIViewController {
    // MARK: - Dependencies
    var apiService: (CalculatorHackatonAPIProtocol & CarLoanHackatonAPIProtocol)!
    var imageService: ImageAPIProtocol!
    
    // MARK: - Public Variables
    weak var delegate: LoanControllerDelegate?
    
    // MARK: - Private Variables
    private var carModel: CarModel?
    private var calculation: CalculationRequest?
    private var calculatorSettings: CalculatorSettings?
    
    private var debouncedCalculation: (() -> Void)?
    private var finalCalculation: CalculationResponse?
    
    private lazy var priceFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = 0
        formatter.locale = Locale(identifier: "ru_RU")
        
        return formatter
    }()
    
    @IBOutlet private weak var scrollView: UIScrollView!
    
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var subtitleLabel: UILabel!
    
    @IBOutlet private weak var initialFeeSlider: ParameterSlider! {
        didSet { initialFeeSlider.currentValueUpdated = update(initialFree:) }
    }
    @IBOutlet private weak var termSlider: ParameterSlider! {
        didSet { termSlider.currentValueUpdated = update(term:) }
    }
    
    @IBOutlet private weak var kaskoSwitch: ParameterSwitch! {
        didSet {
            kaskoSwitch.isHidden = true
            kaskoSwitch.switchOnUpdated = update(switch:)
        }
    }
    @IBOutlet private weak var insuranceSwitch: ParameterSwitch! {
        didSet {
            insuranceSwitch.isHidden = true
            insuranceSwitch.switchOnUpdated = update(switch:)
        }
    }
    @IBOutlet private weak var fullDocumentsSwitch: ParameterSwitch! {
        didSet {
            fullDocumentsSwitch.isHidden = true
            fullDocumentsSwitch.switchOnUpdated = update(switch:)
        }
    }
    @IBOutlet private weak var residualPaymentSwitch: ParameterSwitch!
    @IBOutlet private weak var residualPaymentPromptView: PromptView!
    
    @IBOutlet weak var paymentAmountLabel: UILabel!
    @IBOutlet weak var loanAmountLabel: UILabel!
    
    @IBOutlet private weak var requestButton: UIButton! {
        didSet { requestButton.layer.cornerRadius = 8.0 }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.titleView = UIView()
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "calendar"), style: .plain, target: self, action: #selector(showPaymentsGraph))
        
        navigationController?.navigationBar.barTintColor = .systemBackground
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.layoutIfNeeded()
    }
    
    @objc func showPaymentsGraph() {
        guard let finalCalculation = finalCalculation else {
            return
        }
        let result = finalCalculation.result
        
        let graphRequest: GraphRequest = GraphRequest(
            contractRate: result.contractRate,
            lastPayment: result.lastPayment ?? 0,
            loanAmount: Double(result.loanAmount),
            payment: Double(result.payment),
            term: result.term
        )
        
        firstly {
            apiService.paymentsGraph(graphRequest)
        }.done { graphResponse in
            self.delegate?.loanController(self, didRequestPaymentsGraph: graphResponse.payments)
        }.catch { error in
            print(error)
        }
    }
    
    func configureUI() {
        if let carModel = carModel {
            titleLabel.text = "\(carModel.brand.title) \(carModel.title)"
            subtitleLabel.text = carModel.description
        }
        
        if let renderPhotos = carModel?.renderPhotos, let renderPhoto = renderPhotos["render_widget_left"]?.first?.value {
            imageService.loadImage(with: renderPhoto.path).done { [weak self] imageData in
                self?.imageView.image = UIImage(data: imageData)
            }.catch { error in }
        }
        
        if let calculation = calculation {
            initialFeeSlider.title = "Первый взнос"
            initialFeeSlider.formatStyle = .currency
            initialFeeSlider.subtitle = "\(calculation.maximumInitialFee())"
            initialFeeSlider.minValue = calculation.minimumInitialFree()
            initialFeeSlider.maxValue = calculation.maximumInitialFee()
            initialFeeSlider.currentValue = calculation.initialFee
            
            termSlider.title = "Срок кредита"
            termSlider.formatStyle = .none
            termSlider.subtitle = "\(calculation.maximumTerm()) лет"
            termSlider.minValue = calculation.minimumTerm()
            termSlider.maxValue = calculation.maximumTerm()
            termSlider.currentValue = calculation.term
        }
        
        configureSpecialConditionsSwitches()
        configureResidualPaymentSwitch()
        
        debouncedCalculate()
    }
    
    func configureSpecialConditionsSwitches() {
        if let calculationSettings = calculatorSettings {
            calculationSettings.specialConditions.forEach { specialCondition in
                switch specialCondition.id {
                case .kasko:
                    configureParameterSwitch(kaskoSwitch, with: specialCondition)
                case .insurance:
                    configureParameterSwitch(insuranceSwitch, with: specialCondition)
                case .fullDocuments:
                    configureParameterSwitch(fullDocumentsSwitch, with: specialCondition)
                }
            }
        } else {
            Condition.allCases.forEach {
                switch $0 {
                case .kasko:
                    configureParameterSwitch(kaskoSwitch, with: nil)
                case .insurance:
                    configureParameterSwitch(insuranceSwitch, with: nil)
                case .fullDocuments:
                    configureParameterSwitch(fullDocumentsSwitch, with: nil)
                }
            }
        }
    }
    
    func configureParameterSwitch(_ parameterSwitch: ParameterSwitch, with specialCondition: SpecialCondition?) {
        if let specialCondition = specialCondition {
            parameterSwitch.title = specialCondition.name
            parameterSwitch.switchOn = specialCondition.isChecked
        }
        parameterSwitch.isHidden = specialCondition == nil
    }
    
    func configureResidualPaymentSwitch() {
        residualPaymentSwitch.title = "Остаточный платеж"
        residualPaymentSwitch.switchOn = false
        residualPaymentSwitch.switchOnUpdated = { [weak self] switchOn in
            guard let strongSelf = self else {
                return
            }
            
            if switchOn {
                // Скрываем подсказку и больше не показываем
                strongSelf.residualPaymentPromptView.isHidden = true
                strongSelf.residualPaymentSwitch.detailView = nil
            }
        }
        
        let promptImageView = UIImageView()
        promptImageView.image = UIImage(named: "prompt-icon-image")
        promptImageView.snp.makeConstraints { make in
            make.height.width.equalTo(24.0)
        }
        residualPaymentSwitch.detailView = promptImageView
        
        guard let carModel = carModel else {
            return
        }
        let residualPayment = Double(carModel.minPrice) * 0.648
        residualPaymentPromptView.text = "Планируете продать автомобиль через время? Ориентировочная рыночная стоимость авто через 3 года составит \(priceFormatter.string(from: NSNumber(value: residualPayment)) ?? "")"
        residualPaymentPromptView.pivotX = 182.0
    }
    
    // MARK: - Actions
    func update(initialFree: Int) {
        debouncedCalculate()
    }
    
    func update(term: Int) {
        debouncedCalculate()
    }
    
    func update(switch: Bool) {
        debouncedCalculate()
    }
    
    @IBAction func requestLoan(_ sender: Any) {
        guard let carLoan = makeCarLoan() else {
            return
        }
        self.delegate?.loanController(self, didFinishWorkWithCarLoan: carLoan)
    }
}

extension LoanViewController {
    func configure(withCarModel carModel: CarModel, calculation: CalculationRequest?) {
        self.carModel = carModel
        self.calculation = calculation ?? CalculationRequest.configuredMinimumFeeRequest(with: carModel.minPrice)
        
        firstly {
            apiService.settings(forBrandName: "Haval", language: .ru)
        }.done { calculatorSettings in
            self.calculatorSettings = calculatorSettings
        }.catch { error in
             print(error)
        }
    }
}

// MARK: - Business Login
extension LoanViewController {
    func debouncedCalculate() {
        if debouncedCalculation == nil {
            debouncedCalculation = debounce(interval: 200, queue: DispatchQueue.main, action: calculate)
        }
        debouncedCalculation!()
    }
    
    func calculate() {
        guard let calculationRequest = makeCalculationRequest() else {
            return
        }
        apiService.calculate(calculationRequest).done { calculationResponse in
            let paymentAmount = self.priceFormatter.string(from: NSNumber(value: calculationResponse.result.payment))
            self.paymentAmountLabel.text = "\(paymentAmount ?? "-") / мес"
            
            let loanAmount = self.priceFormatter.string(from: NSNumber(value: calculationResponse.result.loanAmount))
            self.loanAmountLabel.text = "\(loanAmount ?? "-")"
            
            self.finalCalculation = calculationResponse
        }.catch { error in
            print(error)
        }
    }
    
    func makeCalculationRequest() -> CalculationRequest? {
        guard let carModel = carModel else {
            return nil
        }
        
        let cost = carModel.minPrice
        let initialFree = initialFeeSlider.currentValue
        let kaskoValue = Int(Double(cost) * 0.05)
        let residualPayment = 0.0
                
        var specialCondiditions: [String] = []
        if kaskoSwitch.switchOn { specialCondiditions.append(Condition.kasko.rawValue) }
        if insuranceSwitch.switchOn { specialCondiditions.append(Condition.insurance.rawValue) }
        if fullDocumentsSwitch.switchOn { specialCondiditions.append(Condition.fullDocuments.rawValue) }
        
        let term = termSlider.currentValue
        
        return CalculationRequest(
            clientTypes: [],
            cost: cost,
            initialFee: initialFree,
            kaskoValue: kaskoValue,
            language: .ru,
            residualPayment: residualPayment,
            settingsName: "Haval",
            specialConditions: specialCondiditions,
            term: term
        )
    }
    
    func makeCarLoan() -> CarLoan? {
        guard let finalCalculation = finalCalculation, let carModel = carModel else {
            return nil
        }
        
        let currentDate = Date()
        let interestRate = finalCalculation.result.contractRate
        let requestedAmount = finalCalculation.result.loanAmount
        let requestedTerm = finalCalculation.result.term
        let tradeMark = carModel.brand.alias
        let vehicleCost = carModel.minPrice
        
        return CarLoan(
            dateTime: currentDate,
            interestRate: interestRate,
            requestedAmount: requestedAmount,
            requestedTerm: requestedTerm,
            tradeMark: tradeMark,
            vehicleCost: vehicleCost,
            customerParty: nil,
            comment: nil
        )
    }
}
