//
//  LoanControllerDelegate.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 10.10.2020.
//

import Foundation

protocol LoanControllerDelegate: AnyObject {
    func loanController(_ loan: LoanViewController, didRequestPaymentsGraph payments: [Payment])
    func loanController(_ loan: LoanViewController, didFinishWorkWithCarLoan carLoan: CarLoan)
}
