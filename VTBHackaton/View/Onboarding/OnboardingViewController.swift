//
//  OnboardingViewController.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 09.10.2020.
//

import UIKit

class OnboardingViewController: UIViewController {
    @IBOutlet private weak var promoLabel: UILabel! {
        didSet {
            promoLabel.text = "Лучший помощник\n в поиске авто"
        }
    }
}

extension OnboardingViewController {
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.promoLabel.alpha = 0.0
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut) {
            self.promoLabel.alpha = 1.0
            self.promoLabel.isHidden = false
        }
    }
}
