//
//  SuggestControllerDelegate.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 10.10.2020.
//

import Foundation

protocol SuggestControllerDelegate: AnyObject {
    func suggestControllerWillDisappear(_ viewController: SuggestViewController)
    
    func suggestControllerDidDisappear(_ viewController: SuggestViewController)
    
    func suggestController(_ viewController: SuggestViewController, didSelectRecommendedCar carModel: CarModel)
    
    func suggestController(_ viewController: SuggestViewController, didSelectCar carModel: CarModel, withCalculation calculation: (CalculationRequest, CalculationResponse)?)
    
    func suggestControllerWouldBeDismissed(_ viewController: SuggestViewController)
}
