//
//  SuggestPaymentOptionView.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 10.10.2020.
//

import UIKit

import SnapKit

final class SuggestPaymentOptionView: UIView {
    // MARK: Private Variables
    private lazy var cardButton: SmartButton = {
        let button = SmartButton()
        button.layer.cornerRadius = 8.0
        button.layer.borderWidth = 1.0
        button.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.7).cgColor
        button.backgroundColor = .white
        button.clipsToBounds = true
        
        button.addTarget(self, action: #selector(cardButtonDidTapped), for: .touchUpInside)
        button.delegate = self
        return button
    }()
    
    private let contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 15.0
        return stackView
    }()
    
    private let optionTitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        return label
    }()
    
    private let optionPriceLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textAlignment = .right
        return label
    }()
    
    private let priceFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = 0
        formatter.locale = Locale(identifier: "ru_RU")
        
        return formatter
    }()

    // MARK: - Public Variables
    var option: SuggestPaymentOption = .recommended {
        didSet {
            switch option {
            case .recommended:
                optionTitleLabel.text = "Рекомендуем"
                optionTitleLabel.font = .systemFont(ofSize: 16.0, weight: .medium)
                optionTitleLabel.textColor = .label
                optionPriceLabel.font = .systemFont(ofSize: 16.0, weight: .medium)
            case .custom:
                optionTitleLabel.text = "Настроить под себя"
                optionTitleLabel.font = .systemFont(ofSize: 16.0, weight: .regular)
                optionTitleLabel.textColor = .secondaryLabel
                
                optionPriceLabel.isHidden = true
            }
        }
    }
    
    var optionPrice: Int? {
        get {
            if let stringValue = optionPriceLabel.text {
                return Int(stringValue)
            }
            return nil
        }
        set {
            if let newValue = newValue, let formattedValue = priceFormatter.string(from: NSNumber(value: newValue)) {
                optionPriceLabel.text = "от \(formattedValue)/мес"
            } else {
                optionPriceLabel.text = "от - ₽/мес"
            }
        }
    }
    
    var didTapped: (() -> Void)?

    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
}

// MARK: - Private Functions
extension SuggestPaymentOptionView {
    private func setupUI() {
        backgroundColor = UIColor.white
        addSubviews(
            [
                cardButton,
                optionTitleLabel,
                optionPriceLabel
            ]
        )
        setupConstraints()
        
        optionTitleLabel.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        optionPriceLabel.setContentHuggingPriority(.defaultLow, for: .horizontal)
        optionTitleLabel.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        optionPriceLabel.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
    }
    
    private func setupConstraints() {
        setupCardViewConstraints()
        setupOptionTitleLabelConstraints()
        setupOptionPriceLabelConstraints()
    }
    
    private func setupCardViewConstraints() {
        cardButton.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(4.0)
            make.leading.trailing.equalToSuperview().inset(16.0)
            make.height.equalTo(70.0)
        }
    }
    
    private func setupOptionTitleLabelConstraints() {
        optionTitleLabel.snp.makeConstraints { make in
            make.top.bottom.equalTo(cardButton).inset(14.0)
            make.leading.equalTo(cardButton).inset(16.0)
        }
    }
    
    private func setupOptionPriceLabelConstraints() {
        optionPriceLabel.snp.makeConstraints { make in
            make.leading.equalTo(optionTitleLabel.snp.trailing).offset(16.0)
            make.top.bottom.equalTo(cardButton).inset(14.0)
            make.trailing.equalTo(cardButton).inset(16.0)
        }
    }
    
    @objc
    private func cardButtonDidTapped() {
        didTapped?()
    }
}

extension SuggestPaymentOptionView: SmartButtonDelegate {
    func smartButton(_ smartButton: SmartButton, didHighlight highlight: Bool) {        
        cardButton.layer.borderWidth = highlight ? 2.0 : 1.0
        cardButton.layer.borderColor = highlight ? UIColor(named: "AccentColor")!.cgColor : UIColor.lightGray.withAlphaComponent(0.7).cgColor
    }
}
