//
//  SuggestCarView.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 10.10.2020.
//

import UIKit

import SnapKit

final class SuggestCarView: UIView {
    // MARK: - Private Variables
    private let carNameLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 18.0, weight: .medium)
        label.textColor = UIColor.black
        label.numberOfLines = 2
        label.textAlignment = .center
        return label
    }()
    
    private let carImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private let carMinimalPriceLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 20.0, weight: .medium)
        label.textColor = UIColor.black
        label.numberOfLines = 1
        label.textAlignment = .center
        return label
    }()
    
    private lazy var priceFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = 0
        formatter.locale = Locale(identifier: "ru_RU")
        
        return formatter
    }()

    // MARK: - Public Variables
    var viewModel: SuggestCarViewModel? {
        didSet {
            carName = viewModel?.name
            carMinimalPrice = viewModel?.minimalPrice
            carImage = viewModel?.image
            
            viewModel?.imageDidUpdate = { [weak self] image in
                self?.carImage = image
            }
        }
    }
    
    var carName: String? {
        get {
            carNameLabel.text
        }
        set {
            carNameLabel.text = newValue
        }
    }
    
    var carImage: UIImage? {
        get {
            carImageView.image
        }
        set {
            carImageView.image = newValue
        }
    }
    
    var carMinimalPrice: Int? {
        get {
            if let stringValue = carMinimalPriceLabel.text {
                return Int(stringValue)
            }
            return nil
        }
        set {
            if let newValue = newValue, let formattedValue = priceFormatter.string(from: NSNumber(value: newValue)) {
                carMinimalPriceLabel.text = "от \(formattedValue)"
            } else {
                carMinimalPriceLabel.text = "от - ₽"
            }
        }
    }

    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
}

// MARK: - Private Functions
extension SuggestCarView {
    private func setupUI() {
        backgroundColor = UIColor.white
        addSubviews(
            [
                carNameLabel,
                carImageView,
                carMinimalPriceLabel
            ]
        )
        setupConstraints()
    }
    
    private func setupConstraints() {
        setupCarNameLabelConstraints()
        setupCarImageViewConstraints()
        setupCarMinimalPriceLabelLabelConstraints()
    }
    
    private func setupCarNameLabelConstraints() {
        carNameLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(8.0)
            make.leading.equalToSuperview().offset(16.0)
            make.trailing.equalToSuperview().inset(16.0)
        }
    }
    
    private func setupCarImageViewConstraints() {
        carImageView.snp.makeConstraints { make in
            make.top.equalTo(carNameLabel.snp.bottom).inset(4.0)
            make.leading.equalToSuperview().offset(16.0)
            make.trailing.equalToSuperview().inset(16.0)
            make.height.equalTo(160.0)
        }
    }
    
    private func setupCarMinimalPriceLabelLabelConstraints() {
        carMinimalPriceLabel.snp.makeConstraints { make in
            make.top.equalTo(carImageView.snp.bottom).inset(16.0)
            make.leading.equalToSuperview().offset(16.0)
            make.trailing.equalToSuperview().inset(16.0)
            make.bottom.equalToSuperview().inset(20.0)
        }
    }
}
