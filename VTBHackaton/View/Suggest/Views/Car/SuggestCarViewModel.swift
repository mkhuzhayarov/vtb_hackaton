//
//  SuggestCarViewModel.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 10.10.2020.
//

import UIKit

class SuggestCarViewModel {
    var name: String?
    var minimalPrice: Int?
    
    var image: UIImage? {
        didSet {
            imageDidUpdate?(image)
        }
    }
    
    var imageDidUpdate: ((UIImage?) -> Void)?
}
