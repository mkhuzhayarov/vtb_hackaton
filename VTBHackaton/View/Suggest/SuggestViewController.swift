//
//  SuggestViewController.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 10.10.2020.
//

import UIKit

import PanModal
import PromiseKit

class SuggestViewController: UIViewController {
    // MARK: - Dependencies
    var apiService: (MarketplaceHackatonAPIProtocol & CalculatorHackatonAPIProtocol)!
    var imageService: ImageAPIProtocol!
    
    // MARK: - Public Variables
    weak var delegate: SuggestControllerDelegate?
    
    // MARK: - Private Variables
    // Business Models
    private var carModel: CarModel? {
        didSet {
            if let carModel = carModel {
                let viewModel = SuggestCarViewModel()
                viewModel.name = [carModel.brand.title, carModel.model.title].joined(separator: " ")
                viewModel.minimalPrice = carModel.minPrice
                
                if let renderPath = carModel.renderPhotos["side"]?.first?.value.path {
                    imageService.loadImage(with: renderPath).done { imageData in
                        viewModel.image = UIImage(data: imageData)
                    }.catch { error in }
                }
                
                self.carViewModel = viewModel
            } else {
                self.carViewModel = nil
            }
        }
    }
    private var otherCarModels: [CarModel]? {
        didSet {
            self.otherCarViewModels = otherCarModels?.map {
                let viewModel = SuggestCarViewModel()
                viewModel.name = [$0.brand.title, $0.model.title].joined(separator: " ")
                viewModel.minimalPrice = $0.minPrice
                
                if let renderPath = $0.renderPhotos["main_left"]?.first?.value.path {
                    imageService.loadImage(with: renderPath).done { imageData in
                        viewModel.image = UIImage(data: imageData)
                    }.catch { error in }
                }
                
                return viewModel
            } ?? []
        }
    }
    private var calculation: (request: CalculationRequest, response: CalculationResponse)?
    
    // View Models
    private var carViewModel: SuggestCarViewModel?
    private var otherCarViewModels: [SuggestCarViewModel] = []
    
    @IBOutlet private weak var stackView: UIStackView?
    
    // Car
    private let carView: SuggestCarView = SuggestCarView()
    
    // Options
    private let recommendedOptionView: SuggestPaymentOptionView = {
        let optionView = SuggestPaymentOptionView()
        optionView.option = .recommended
        optionView.optionPrice = nil
        
        return optionView
    }()
    private let customOptionView: SuggestPaymentOptionView = {
        let optionView = SuggestPaymentOptionView()
        optionView.option = .custom
        optionView.optionPrice = nil
        
        return optionView
    }()
    
    private lazy var minimalMonthlyPaymentOptionNoteContainer: UIView = UIView.container(
        for: minimalMonthlyPaymentOptionNoteLabel,
        withInsets: UIEdgeInsets(
            top: 2.0,
            left: 26.0,
            bottom: 0.0,
            right: 16.0
        )
    )
    
    private let minimalMonthlyPaymentOptionNoteLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12.0, weight: .regular)
        label.textColor = UIColor.secondaryLabel
        label.text = "* Страхование жизни, КАСКО и ТО включено"
        return label
    }()
    
    // Recommendations
    private lazy var recommendationsHeaderContainer: UIView = UIView.container(
        for: recommendationsHeaderLabel,
        withInsets: UIEdgeInsets(
            top: 0.0,
            left: 32.0,
            bottom: 6.0,
            right: 16.0
        )
    )
    
    private let recommendationsHeaderLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12.0, weight: .regular)
        label.textColor = UIColor(named: "AccentColor")
        label.text = "Другие предложения"
        return label
    }()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(
            frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 80.0),
            collectionViewLayout: collectionViewFlowLayout
        )
        collectionView.backgroundColor = .white
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        
        collectionView.snp.makeConstraints { make in
            make.height.equalTo(80.0)
        }
        
        collectionView.register(
            SuggestCarCollectionViewCell.self,
            forCellWithReuseIdentifier: "SuggestCarCollectionViewCell"
        )
        return collectionView
    }()
    
    private let collectionViewFlowLayout: UICollectionViewFlowLayout = {
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = .horizontal
        collectionViewFlowLayout.sectionHeadersPinToVisibleBounds = true
        collectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 0.0, left: 16.0, bottom: 0.0, right: 16.0)
        collectionViewFlowLayout.minimumInteritemSpacing = 12.0
        collectionViewFlowLayout.minimumLineSpacing = 12.0
        collectionViewFlowLayout.itemSize = CGSize(width: 168, height: 80)
        return collectionViewFlowLayout
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }
}

extension SuggestViewController {
    private func configureUI() {
        view.backgroundColor = .white
        
        updateCarSection()
        updateOptionsSection()
        updateRecommendationsSection()
        
        let footerView = UIView()
        footerView.setContentHuggingPriority(.init(249), for: .vertical)
        stackView?.addArrangedSubview(footerView)
    }
    
    private func updateCarSection() {
        carView.viewModel = carViewModel
        
        if carView.superview == nil {
            stackView?.insertArrangedSubview(carView, at: 0)
            stackView?.setCustomSpacing(12.0, after: carView)
        }
    }
    
    private func updateOptionsSection() {
        recommendedOptionView.optionPrice = calculation?.response.result.payment
        recommendedOptionView.didTapped = { [weak self] in
            guard let self = `self`, let carModel = self.carModel else {
                return
            }
            self.delegate?.suggestController(self, didSelectCar: carModel, withCalculation: self.calculation)
        }
        
        customOptionView.didTapped = { [weak self] in
            guard let self = `self`, let carModel = self.carModel else {
                return
            }
            self.delegate?.suggestController(self, didSelectCar: carModel, withCalculation: nil)
        }
        
        if recommendedOptionView.superview == nil {
            stackView?.insertArrangedSubview(recommendedOptionView, at: 1)
        }
        if customOptionView.superview == nil {
            stackView?.insertArrangedSubview(customOptionView, at: 2)
            stackView?.insertArrangedSubview(
                minimalMonthlyPaymentOptionNoteContainer,
                at: 3
            )
        }
        stackView?.setCustomSpacing(24.0, after: minimalMonthlyPaymentOptionNoteContainer)
    }
    
    private func updateRecommendationsSection() {
        guard let stackView = stackView else {
            return
        }
        if otherCarViewModels.isEmpty {
            if collectionView.superview != nil {
                stackView.removeArrangedSubview(recommendationsHeaderContainer)
                stackView.removeArrangedSubview(collectionView)
                
                recommendationsHeaderLabel.removeFromSuperview()
                collectionView.removeFromSuperview()
            }
        } else {
            if collectionView.superview == nil {
                stackView.insertArrangedSubview(
                    recommendationsHeaderContainer,
                    at: stackView.arrangedSubviews.count - 1
                )
                stackView.insertArrangedSubview(
                    collectionView,
                    at: stackView.arrangedSubviews.count - 1
                )
            }
            
            collectionView.reloadData()
        }
    }
}

extension SuggestViewController {
    func configure(withCarModel carModel: CarModel) {
        self.carModel = carModel
        self.updateCarSection()
        
        let calculationRequest = CalculationRequest.configuredAverageFeeRequest(with: carModel.minPrice)
        firstly {
            apiService.calculate(calculationRequest)
        }.done { calculationResponse in
            self.calculation = (calculationRequest, calculationResponse)
            self.updateOptionsSection()
        }.catch { error in
            self.delegate?.suggestControllerWouldBeDismissed(self)
        }
        
        firstly {
            apiService.marketplace(ofBrandName: carModel.brand.alias, modelName: nil)
        }.done { models in
            self.otherCarModels = models.filter {
                $0.model.alias != carModel.model.alias
            }
            self.updateRecommendationsSection()
            self.panModalSetNeedsLayoutUpdate()
        }.catch { error in
            self.delegate?.suggestControllerWouldBeDismissed(self)
        }
    }
}

extension SuggestViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        otherCarViewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: "SuggestCarCollectionViewCell",
            for: indexPath
        ) as! SuggestCarCollectionViewCell
        cell.viewModel = otherCarViewModels[indexPath.item]
        return cell
    }
}

extension SuggestViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let model = otherCarModels![indexPath.item]
        delegate?.suggestController(self, didSelectRecommendedCar: model)
    }
}

extension SuggestViewController: PanModalPresentable {
    var panScrollable: UIScrollView? {
        nil
    }
    
    var cornerRadius: CGFloat {
        17.0
    }
    
    var shortFormHeight: PanModalHeight {
        .contentHeight(212.0)
    }
    
    var longFormHeight: PanModalHeight {
        .intrinsicHeight
    }
    
    var shouldRoundTopCorners: Bool {
        true
    }
    
    func panModalWillDismiss() {
        delegate?.suggestControllerWillDisappear(self)
    }
    
    func panModalDidDismiss() {
        delegate?.suggestControllerDidDisappear(self)
    }
}
