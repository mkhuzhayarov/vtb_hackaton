//
//  SuggestCarCollectionViewCell.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 10.10.2020.
//

import UIKit

final class SuggestCarCollectionViewCell: UICollectionViewCell {
    // MARK: Private Variables
    private let carImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private let labelsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 4.0
        return stackView
    }()
    
    private let carNameLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 10.0, weight: .regular)
        label.textColor = UIColor.black
        label.numberOfLines = 1
        return label
    }()
    
    private let carMinimalPriceLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 10.0, weight: .regular)
        label.textColor = UIColor.secondaryLabel
        label.numberOfLines = 1
        return label
    }()
    
    private let priceFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = 0
        formatter.locale = Locale(identifier: "ru_RU")
        
        return formatter
    }()

    // MARK: - Public Variables
    var viewModel: SuggestCarViewModel? {
        didSet {
            carName = viewModel?.name
            carMinimalPrice = viewModel?.minimalPrice
            carImage = viewModel?.image
            
            viewModel?.imageDidUpdate = { [weak self] image in
                self?.carImage = image
            }
        }
    }
    
    var carName: String? {
        get {
            carNameLabel.text
        }
        set {
            carNameLabel.text = newValue?.uppercased()
        }
    }
    
    var carImage: UIImage? {
        get {
            carImageView.image
        }
        set {
            carImageView.image = newValue
        }
    }
    
    var carMinimalPrice: Int? {
        get {
            if let stringValue = carMinimalPriceLabel.text {
                return Int(stringValue)
            }
            return nil
        }
        set {
            if let newValue = newValue, let formattedValue = priceFormatter.string(from: NSNumber(value: newValue)) {
                carMinimalPriceLabel.text = "от \(formattedValue)"
            } else {
                carMinimalPriceLabel.text = "от - ₽"
            }
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            set(highlighted: isHighlighted)
        }
    }
    
    override var isSelected: Bool {
        didSet {
            set(selected: isSelected)
        }
    }

    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
}

// MARK: - Private Functions
extension SuggestCarCollectionViewCell {
    private func setupUI() {
        contentView.backgroundColor = UIColor.white
        contentView.layer.borderWidth = 1.0
        contentView.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.7).cgColor
        contentView.layer.cornerRadius = 8.0
        contentView.addSubviews(
            [
                carImageView,
                labelsStackView
            ]
        )
        contentView.clipsToBounds = true
        
        labelsStackView.addArrangedSubview(carNameLabel)
        labelsStackView.addArrangedSubview(carMinimalPriceLabel)
        setupConstraints()
    }
    
    private func setupConstraints() {
        setupCarImageViewConstraints()
        setupLabelsStackViewConstraints()
    }
    
    private func setupCarImageViewConstraints() {
        carImageView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(-50.0)
            make.top.bottom.equalToSuperview().inset(8.0)
            make.width.equalTo(120.0)
        }
    }
    
    private func setupLabelsStackViewConstraints() {
        labelsStackView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalTo(carImageView.snp.trailing).offset(-8.0)
            make.trailing.equalToSuperview().inset(12.0)
        }
    }
    
    private func set(highlighted: Bool) {
        if highlighted {
            contentView.layer.borderWidth = 2.0
            contentView.layer.borderColor = UIColor(named: "AccentColor")?.cgColor
        } else {
            contentView.layer.borderWidth = 1.0
            contentView.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.7).cgColor
        }
    }
    
    private func set(selected: Bool) {
        if selected {
            contentView.layer.borderColor = UIColor(named: "AccentColor")?.cgColor
        } else {
            contentView.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.7).cgColor
        }
    }
}
