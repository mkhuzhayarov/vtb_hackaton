//
//  SuggestPaymentOption.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 10.10.2020.
//

import Foundation

enum SuggestPaymentOption {
    case recommended
    case custom
}
