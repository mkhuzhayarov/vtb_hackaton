//
//  FormViewDelegate.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 10.10.2020.
//

import Foundation

protocol FormControllerDelegate: AnyObject {
    func formController(_ form: FormViewController, didFinishWorkWithCarLoanResponse carLoan: CarLoanResponse)
}
