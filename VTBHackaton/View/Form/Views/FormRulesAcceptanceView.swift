//
//  FormRulesAcceptanceView.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 11.10.2020.
//

import UIKit

final class FormRulesAcceptanceView: UIView {
    // MARK: - Private Properties
    private lazy var checkBoxButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "checkbox-on-image"), for: .selected)
        button.setImage(UIImage(named: "checkbox-off-image"), for: .normal)
        
        button.addTarget(self, action: #selector(didChecked(_:)), for: .touchUpInside)
        return button
    }()
    
    private let rulesTextLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 14.0, weight: .regular)
        label.textColor = .label
        label.numberOfLines = 0
        label.text = "Я согласен с условиями договора и подтверждаю свое согласие на обработку персональных данных Банком"
        return label
    }()
    
    private lazy var openRulesButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = .systemFont(ofSize: 16.0, weight: .regular)
        
        button.setTitle("Открыть список документов", for: .normal)
        button.setTitleColor(UIColor(named: "AccentColor"), for: .normal)
        button.contentHorizontalAlignment = .leading
        
        button.addTarget(self, action: #selector(didRulesTapped), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Public Properties
    var isAccepted: Bool {
        get {
            checkBoxButton.isSelected
        }
        set {
            checkBoxButton.isSelected = newValue
        }
    }
    
    var didAccept: ((Bool) -> Void)?
    
    var didOpenRulesTapped: (() -> Void)?
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
}

extension FormRulesAcceptanceView {
    private func setupUI() {
        addSubviews(
            [
                checkBoxButton,
                rulesTextLabel,
                openRulesButton
            ]
        )
        
        checkBoxButton.snp.makeConstraints { make in
            make.width.height.equalTo(24.0)
            make.top.equalToSuperview().inset(12.0)
            make.leading.equalToSuperview()
        }
        
        rulesTextLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(12.0)
            make.leading.equalTo(checkBoxButton.snp.trailing).offset(16.0)
            make.trailing.equalToSuperview()
        }
        
        openRulesButton.snp.makeConstraints { make in
            make.height.equalTo(24.0)
            make.leading.equalTo(rulesTextLabel.snp.leading)
            make.top.equalTo(rulesTextLabel.snp.bottom).offset(12.0)
            make.bottom.equalToSuperview().inset(12.0)
        }
        
        rulesTextLabel.setContentHuggingPriority(.init(249), for: .vertical)
        rulesTextLabel.setContentHuggingPriority(.init(251), for: .horizontal)
        rulesTextLabel.setContentCompressionResistancePriority(.init(249), for: .vertical)
    }
    
    @objc
    private func didChecked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        didAccept?(isAccepted)
    }
    
    @objc
    private func didRulesTapped(_ sender: UIButton) {
        didOpenRulesTapped?()
    }
}
