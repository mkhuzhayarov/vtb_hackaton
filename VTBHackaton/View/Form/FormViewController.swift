//
//  FormViewController.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 09.10.2020.
//

import UIKit

import AnimatedField
import PromiseKit

class FormViewController: UIViewController {
    // MARK: - Dependencies
    var apiService: CarLoanHackatonAPIProtocol!
    
    // MARK: - Nested Types
    enum Field: Int {
        case name
        case birthDate
        case birthPlace
        case nationality
        case gender
        case clientIncome
    }
    
    // MARK: - Dependencies
    var sessionService: SessionService!
    
    // MARK: - Public Varibales
    weak var delegate: FormControllerDelegate?
    
    // MARK: - Private Variables
    private let fieldFormat: AnimatedFieldFormat = {
        var fieldFormat = AnimatedFieldFormat()
        fieldFormat.titleAlwaysVisible = false
        fieldFormat.titleFont = .systemFont(ofSize: 16, weight: .medium)
        fieldFormat.textFont = .systemFont(ofSize: 16, weight: .medium)
        fieldFormat.titleColor = .secondaryLabel
        fieldFormat.textColor = .label
        fieldFormat.lineColor = .secondaryLabel
        fieldFormat.highlightColor = UIColor(named: "AccentColor")
        return fieldFormat
    }()
    
    @IBOutlet private weak var nameField: AnimatedField! {
        didSet {
            nameField.format = fieldFormat
            nameField.placeholder = "ФИО"
            nameField.type = .text(3, 50)
            nameField.autocapitalizationType = .words
            nameField.delegate = self
            nameField.dataSource = self
            nameField.tag = Field.name.rawValue
        }
    }
    @IBOutlet private weak var birthDateField: AnimatedField! {
        didSet {
            birthDateField.format = fieldFormat
            birthDateField.placeholder = "Дата рождения"
            birthDateField.type = .datepicker(.date, Date(), nil, Date(), "Готово", "dd MMMM yyyy", Locale(identifier: "ru_RU"))
            birthDateField.delegate = self
            birthDateField.dataSource = self
            birthDateField.tag = Field.birthDate.rawValue
        }
    }
    @IBOutlet private weak var birthPlaceField: AnimatedField! {
        didSet {
            birthPlaceField.format = fieldFormat
            birthPlaceField.placeholder = "Место рождения"
            birthPlaceField.type = .text(3, 20)
            birthPlaceField.autocapitalizationType = .words
            birthPlaceField.delegate = self
            birthPlaceField.dataSource = self
            birthPlaceField.tag = Field.birthPlace.rawValue
        }
    }
    @IBOutlet private weak var nationalityField: AnimatedField! {
        didSet {
            nationalityField.format = fieldFormat
            nationalityField.placeholder = "Национальность"
            nationalityField.type = .text(2, 15)
            nationalityField.autocapitalizationType = .words
            nationalityField.delegate = self
            nationalityField.dataSource = self
            nationalityField.tag = Field.nationality.rawValue
        }
    }
    @IBOutlet private weak var genderField: AnimatedField! {
        didSet {
            genderField.format = fieldFormat
            genderField.placeholder = "Пол"
            genderField.type = .stringpicker(["Мужчина", "Женщина", "Неизвестно"], "Готово")
            genderField.delegate = self
            genderField.dataSource = self
            genderField.tag = Field.gender.rawValue
        }
    }
    @IBOutlet private weak var clientIncomeField: AnimatedField! {
        didSet {
            clientIncomeField.format = fieldFormat
            clientIncomeField.placeholder = "Доход клиента"
            clientIncomeField.type = .number(2, 15, "Готово")
            clientIncomeField.delegate = self
            clientIncomeField.dataSource = self
            clientIncomeField.tag = Field.clientIncome.rawValue
        }
    }
    @IBOutlet private weak var rulesAcceptanceView: FormRulesAcceptanceView! {
        didSet {
            rulesAcceptanceView.isAccepted = false
            rulesAcceptanceView.didAccept = { [weak self] accepted in
                self?.sessionService.set(rulesAccepted: accepted)
            }
            rulesAcceptanceView.didOpenRulesTapped = {
                // ...
            }
        }
    }
    
    @IBOutlet private weak var requestButton: UIButton! {
        didSet {
            requestButton.layer.cornerRadius = 8.0
        }
    }
    
    private let currencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = 0
        formatter.currencySymbol = "₽"
        formatter.locale = Locale(identifier: "ru_RU")
        
        return formatter
    }()
    
    private var carLoan: CarLoan?
    
    // MARK: - Action
    @IBAction private func requestLoan(_ sender: Any) {
        guard sessionService.isComplete, let carLoan = carLoan else {
            let feedbackGenerator = UINotificationFeedbackGenerator()
            feedbackGenerator.prepare()
            feedbackGenerator.notificationOccurred(.error)
            return
        }
        
        let ownFormatter = DateFormatter()
        ownFormatter.dateFormat = "yyyy-MM-dd"
        ownFormatter.locale = Locale(identifier: "ru_RU")
        
        guard let rawDate = sessionService.dateFormatter.date(from: sessionService.birthDateTime!) else {
            return
        }
        let formattedDate = ownFormatter.string(from: rawDate)
        
        let person = Person(
            birthDateTime: formattedDate,
            birthPlace: sessionService.birthPlace!,
            familyName: sessionService.familyName!,
            firstName: sessionService.firstName!,
            middleName: sessionService.middleName ?? "",
            gender: Gender(rawValue: sessionService.gender!)!,
            nationalityCode: "RU"
        )
        
        let customerParty = CustomerParty(
            person: person,
            email: "email@gmail.com",
            phone: "+79252181709",
            incomeAmount: sessionService.clientIncome!
        )
        
        let carLoanRequest = CarLoan(
            dateTime: carLoan.dateTime,
            interestRate: carLoan.interestRate,
            requestedAmount: carLoan.requestedAmount,
            requestedTerm: carLoan.requestedTerm,
            tradeMark: carLoan.tradeMark,
            vehicleCost: carLoan.vehicleCost,
            customerParty: customerParty,
            comment: "Отсутствует"
        )
        
        firstly {
            apiService.loanCar(carLoanRequest)
        }.done { carLoanResponse in
            self.delegate?.formController(self, didFinishWorkWithCarLoanResponse: carLoanResponse)
        }.catch { error in
            
        }
    }
}

extension FormViewController {
    func configure(withCarLoan carLoan: CarLoan) {
        self.carLoan = carLoan
    }
}

extension FormViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Оформление заявки"
        restoreState()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        sessionService.set(rulesAccepted: false)
    }
}

extension FormViewController {
    func restoreState() {
        nameField.text = [
            sessionService.familyName,
            sessionService.firstName,
            sessionService.middleName
        ].compactMap {$0 }.joined(separator: " ")
        nameField.textFieldDidBeginEditing(nameField.textField)
        nameField.textFieldDidEndEditing(nameField.textField)
        
        if let birthDateTime = sessionService.birthDateTime {
            birthDateField.text = birthDateTime
            birthDateField.textFieldDidBeginEditing(birthDateField.textField)
            birthDateField.textFieldDidEndEditing(birthDateField.textField)
        }
        
        birthPlaceField.text = sessionService.birthPlace
        birthPlaceField.textFieldDidBeginEditing(birthPlaceField.textField)
        birthPlaceField.textFieldDidEndEditing(birthPlaceField.textField)
        
        nationalityField.text = sessionService.nationality
        nationalityField.textFieldDidBeginEditing(nationalityField.textField)
        nationalityField.textFieldDidEndEditing(nationalityField.textField)
        
        if let genderString = sessionService.gender, let gender = Gender(rawValue: genderString) {
            switch gender {
            case .male:
                genderField.text = "Мужчина"
            case .female:
                genderField.text = "Женщина"
            case .unknown:
                genderField.text = "Неизвестно"
            }
            genderField.textFieldDidBeginEditing(genderField.textField)
            genderField.textFieldDidEndEditing(genderField.textField)
        }
        if let clientIncome = sessionService.clientIncome {
            clientIncomeField.text = currencyFormatter.string(from: NSNumber(value: clientIncome))
            clientIncomeField.textFieldDidBeginEditing(clientIncomeField.textField)
            clientIncomeField.textFieldDidEndEditing(clientIncomeField.textField)
        }
        
        rulesAcceptanceView.isAccepted = false
    }
}

extension FormViewController: AnimatedFieldDelegate {
    func animatedFieldDidBeginEditing(_ animatedField: AnimatedField) {
        guard let text = animatedField.text, !text.isEmpty else {
            return
        }
        
        guard let fieldType = Field(rawValue: animatedField.tag) else {
            return
        }
        
        if fieldType == .clientIncome {
            animatedField.text = currencyFormatter.number(from: text)?.stringValue
        }
    }
    
    func animatedFieldDidEndEditing(_ animatedField: AnimatedField) {
        guard let text = animatedField.text, !text.isEmpty else {
            return
        }
        
        guard let fieldType = Field(rawValue: animatedField.tag) else {
            return
        }
        
        switch fieldType {
        case .name:
            if !sessionService.save(name: text) {
                animatedField.showAlert()
            }
            
        case .birthDate:
            if !sessionService.save(birthDate: text) {
                animatedField.showAlert()
            }
            
        case .birthPlace:
            if !sessionService.save(birthPlace: text) {
                animatedField.showAlert()
            }
            
        case .nationality:
            if !sessionService.save(nationality: text) {
                animatedField.showAlert()
            }
            
        case .gender:
            if !sessionService.save(gender: text) {
                animatedField.showAlert()
            }
            
        case .clientIncome:
            if let number = NumberFormatter().number(from: text) {
                animatedField.text = currencyFormatter.string(from: number)
            }
            
            if !sessionService.save(clientIncome: text) {
                animatedField.showAlert()
            }
        }
    }
}

extension FormViewController: AnimatedFieldDataSource {
    func animatedFieldShouldReturn(_ animatedField: AnimatedField) -> Bool {
        // Try to find next responder
        if let field = Field(rawValue: animatedField.tag) {
            switch field {
            case .name:
                _ = birthDateField.becomeFirstResponder()
            case .birthDate:
                _ = birthPlaceField.becomeFirstResponder()
            case .birthPlace:
                _ = nationalityField.becomeFirstResponder()
            case .nationality:
                _ = genderField.becomeFirstResponder()
            case .gender:
                _ = clientIncomeField.becomeFirstResponder()
            case .clientIncome:
                _ = animatedField.resignFirstResponder()
            }
        }
        return true
    }
}
