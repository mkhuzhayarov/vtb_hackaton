//
//  DecisionViewController.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 10.10.2020.
//

import UIKit

class DecisionViewController: UIViewController {
    // MARK: - Public Variables
    weak var delegate: DecisionControllerDelegate?
    
    // MARK: - Private Variables
    @IBOutlet weak var decisionLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    private var carLoanDecision: CarLoanResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let carLoanDecision = carLoanDecision else {
            return
        }
        
        let userText: String?
        switch carLoanDecision.application.decision.status {
        case .prescoreApproved:
            userText = "Ваша заявка одобрена!"
        case .prescoreDenied:
            userText = "Ваша заявка отклонена :("
            descriptionLabel.isHidden = true
        case .processing:
            userText = "Ваша заявка в обработке"
            descriptionLabel.isHidden = true
        }
        decisionLabel.text = userText ?? "Ваша заявка принята!"
    }
}

extension DecisionViewController {
    func configure(withCarLoanDecision carLoanDecision: CarLoanResponse) {
        self.carLoanDecision = carLoanDecision
    }
}

extension DecisionViewController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}
