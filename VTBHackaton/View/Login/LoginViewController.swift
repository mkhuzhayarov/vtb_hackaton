//
//  LoginViewController.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 09.10.2020.
//

import UIKit

import AnimatedField

class LoginViewController: UIViewController {
    // MARK: - Dependencies
    
    // MARK: - Public Variables
    weak var delegate: LoginControllerDelegate?
    
    // MARK: - Private Variables
    private var carLoan: CarLoan?
    
    private let fieldFormat: AnimatedFieldFormat = {
        var fieldFormat = AnimatedFieldFormat()
        fieldFormat.titleAlwaysVisible = false
        fieldFormat.titleFont = .systemFont(ofSize: 16, weight: .medium)
        fieldFormat.textFont = .systemFont(ofSize: 16, weight: .medium)
        fieldFormat.titleColor = .secondaryLabel
        fieldFormat.textColor = .label
        fieldFormat.lineColor = .secondaryLabel
        fieldFormat.highlightColor = UIColor(named: "AccentColor")
        
        return fieldFormat
    }()

    @IBOutlet private weak var stackView: UIStackView!
    @IBOutlet private weak var phoneTextField: AnimatedField! {
        didSet {
            phoneTextField.format = fieldFormat
            phoneTextField.placeholder = "Номер телефона"
            phoneTextField.type = .phone(17, nil)
            phoneTextField.delegate = self
            phoneTextField.dataSource = self
        }
    }
    
    @IBOutlet private weak var signInButton: UIButton! {
        didSet {
            signInButton.layer.cornerRadius = 8.0
        }
    }
}

extension LoginViewController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        navigationController?.navigationBar.barTintColor = .systemBackground
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.layoutIfNeeded()
    }
}

extension LoginViewController: AnimatedFieldDelegate {
    func animatedFieldDidChange(_ animatedField: AnimatedField) {
        guard let text = animatedField.text else {
            return
        }
        
        if text.count == 17 {
            var phoneText = phone(fromFormattedString: text)
            _ = phoneText.removeFirst()
            
            let phone = Phone(phone: phoneText)
            self.delegate?.loginController(self, didFinishWorkWithPhone: phone)
        }
    }
    
    func animatedFieldDidEndEditing(_ animatedField: AnimatedField) {
        guard let text = animatedField.text, !text.isEmpty else {
            return
        }
    }
}

extension LoginViewController: AnimatedFieldDataSource {
    func animatedField(_ animatedField: AnimatedField,
                       shouldChangeCharactersIn range: NSRange,
                       replacementString string: String) -> Bool? {
        guard let text = animatedField.text else {
            return false
        }
        
        let newText = (text as NSString).replacingCharacters(in: range, with: string)
        let formattedText = formatted(phone: newText)
        animatedField.text = formattedText
        
        animatedFieldDidChange(animatedField)
        
        return false
    }
    
    private func formatted(phone: String) -> String {
        let numbers = phone.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        var result = ""
        var index = numbers.startIndex // numbers iterator

        // iterate over the mask characters until the iterator of numbers ends
        for ch in "+X (XXX) XXX XXXX" where index < numbers.endIndex {
            if ch == "X" {
                // mask requires a number in this place, so take the next one
                result.append(numbers[index])

                // move numbers iterator to the next index
                index = numbers.index(after: index)

            } else {
                result.append(ch) // just append a mask character
            }
        }
        return result
    }
    
    private func phone(fromFormattedString formattedString: String) -> String {
        return formattedString.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
    }
}
