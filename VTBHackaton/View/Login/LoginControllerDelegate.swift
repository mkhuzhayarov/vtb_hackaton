//
//  LoginControllerDelegate.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 11.10.2020.
//

import Foundation

protocol LoginControllerDelegate: AnyObject {
    func loginController(_ login: LoginViewController, didFinishWorkWithPhone phone: Phone)
}
