//
//  SmartButton.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 10.10.2020.
//

import UIKit

protocol SmartButtonDelegate: AnyObject {
    func smartButton(_ smartButton: SmartButton, didHighlight highlight: Bool)
}

final class SmartButton: UIButton {
    weak var delegate: SmartButtonDelegate?
    
    override var isHighlighted: Bool {
        didSet {
            delegate?.smartButton(self, didHighlight: isHighlighted)
        }
    }
}
