//
//  UIView+Extensions.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 10.10.2020.
//

import UIKit

import SnapKit

extension UIView {
    class func container(for view: UIView, withInsets insets: UIEdgeInsets) -> UIView {
        let containerView = UIView()
        containerView.addSubview(view)
        
        view.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(insets)
        }
        
        return containerView
    }
    
    func addSubviews(_ subviews: [UIView]) {
        subviews.forEach { addSubview($0) }
    }
}
