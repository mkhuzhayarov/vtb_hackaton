//
//  GraphViewController.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 10.10.2020.
//

import UIKit

class GraphViewController: UIViewController {
    // MARK: - Dependencies
    var apiService: CalculatorHackatonAPIProtocol!
    
    private let calender: Calendar = Calendar.current
    private let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        dateFormatter.locale = Locale(identifier: "ru_RU")
        
        return dateFormatter
    }()
    
    // MARK: - Private Variables
    private var payments: [Payment]!
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.register(GraphTableViewCell.self, forCellReuseIdentifier: "GraphTableViewCell")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "График платежей"
    }
}

extension GraphViewController {
    func configure(withPayments payments: [Payment]) {
        self.payments = payments
    }
}

extension GraphViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        payments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GraphTableViewCell") as! GraphTableViewCell
        let payment = payments[indexPath.row]
        
        var monthComponent = DateComponents()
        monthComponent.month = payment.order;
        
        guard let paymentDate = calender.date(byAdding: monthComponent, to: Date()) else {
            return cell
        }
        let dateString = dateFormatter.string(from: paymentDate)
        
        cell.textLabel?.text = "\(dateString)"
        cell.subtitleLabel.text = "\(payment.payment) ₽"
        
        return cell
    }
    
    
}
