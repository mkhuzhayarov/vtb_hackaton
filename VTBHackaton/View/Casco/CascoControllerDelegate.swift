//
//  CascoControllerDelegate.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 10.10.2020.
//

import Foundation

protocol CascoControllerDelegate: AnyObject {
    func cascoController(_ casco: CascoViewController, didFinishWorkWithInfo info: Any)
}
