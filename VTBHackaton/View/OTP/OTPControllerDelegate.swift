//
//  OTPControllerDelegate.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 11.10.2020.
//

import Foundation

protocol OTPControllerDelegate: AnyObject {
    func otpController(_ otp: OTPViewController, didFinishWorkWithInfo info: Any)
}
