//
//  OTPViewController.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 09.10.2020.
//

import UIKit

import PromiseKit
import AnimatedField

class OTPViewController: UIViewController {
    // MARK: - Dependencies
    var sessionService: SessionService!
    var apiService: AuthHackatonAPIProtocol!
    
    // MARK: - Public Variables
    weak var delegate: OTPControllerDelegate?
    
    // MARK: - Private Variables
    private var phone: Phone?
    
    private let fieldFormat: AnimatedFieldFormat = {
        var fieldFormat = AnimatedFieldFormat()
        fieldFormat.titleAlwaysVisible = false
        fieldFormat.titleFont = .systemFont(ofSize: 16, weight: .medium)
        fieldFormat.textFont = .systemFont(ofSize: 16, weight: .medium)
        fieldFormat.titleColor = .secondaryLabel
        fieldFormat.textColor = .label
        fieldFormat.lineColor = .secondaryLabel
        fieldFormat.highlightColor = UIColor(named: "AccentColor")
        
        return fieldFormat
    }()

    @IBOutlet private weak var phoneTextField: AnimatedField! {
        didSet {
            phoneTextField.format = fieldFormat
            phoneTextField.placeholder = "СМС-код"
            phoneTextField.type = .price(9999, 4)
            phoneTextField.delegate = self
        }
    }
    
    @IBOutlet private weak var phoneLabel: UILabel!
    @IBOutlet private weak var oneMoreButton: UIButton! {
        didSet {
            oneMoreButton.layer.cornerRadius = 8.0
        }
    }
}

extension OTPViewController {
    func configure(withPhone phone: Phone) {
        self.phone = phone
        self.apiService.requestCode(with: phone)
    }
}

extension OTPViewController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

extension OTPViewController: AnimatedFieldDelegate {
    func animatedFieldDidChange(_ animatedField: AnimatedField) {
        guard let text = animatedField.text, !text.isEmpty else {
            return
        }
        
        guard text.count == 4 else {
            return
        }
        
        animatedField.resignFirstResponder()
        
        guard let phone = phone, let code = Int(text) else {
            return
        }
        
        let phoneCode = PhoneCode(phone: phone.phone, code: code)
        firstly {
            self.apiService.auth(with: phoneCode)
        }.done { (profile, user) in
            self.delegate?.otpController(self, didFinishWorkWithInfo: user)
        }.catch { error in
            print(error)
        }
    }
    
    func animatedFieldDidEndEditing(_ animatedField: AnimatedField) {
        guard let text = animatedField.text, !text.isEmpty else {
            return
        }
        
        
    }
}
