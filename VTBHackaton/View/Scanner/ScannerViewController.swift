//
//  ScannerViewController.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 09.10.2020.
//

import UIKit
import AVFoundation

import PanModal
import PromiseKit

class ScannerViewController: UIViewController {
    // MARK: - Dependencies
    var cameraService: CameraService!
    var apiService: (CarRecognitionHackatonAPIProtocol & MarketplaceHackatonAPIProtocol)!
    
    // MARK: - Public Variables
    weak var delegate: ScannerControllerDelegate?
    
    // MARK: - Private Variables
    @IBOutlet private weak var videoView: UIView!
    private weak var videoLayer: AVCaptureVideoPreviewLayer? {
        didSet {
            oldValue?.removeFromSuperlayer()
            if let videoLayer = videoLayer {
                videoLayer.frame = videoView.bounds
                videoView.layer.addSublayer(videoLayer)
            }
        }
    }
    
    @IBOutlet private weak var captureButton: UIButton! {
        didSet {
            captureButton.layer.cornerRadius = 8.0
        }
    }
    private var shouldMakePhoto = false
    
    @IBAction private func capture(_ sender: Any) {
        shouldMakePhoto = true
    }
}

// MARK: - Lifecycle
extension ScannerViewController {
    override func viewDidLayoutSubviews() {
        videoLayer?.frame = videoView.bounds
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        configureCamera()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.barTintColor = nil
        navigationController?.navigationBar.setBackgroundImage(nil, for:.default)
        navigationController?.navigationBar.shadowImage = nil
        navigationController?.navigationBar.layoutIfNeeded()
        
        startCamera()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopCamera()
    }
}

// MARK: - UI
extension ScannerViewController {
    func configureNavigationBar() {
        navigationItem.title = "Сканирование автомобиля"
    }
}

// MARK: - Camera
extension ScannerViewController {
    public func startCamera() {
        shouldMakePhoto = false
        cameraService.start()
        DispatchQueue.main.async {
            self.captureButton.isEnabled = true
        }
    }
    
    public func stopCamera() {
        cameraService.stop()
        DispatchQueue.main.async {
            self.captureButton.isEnabled = false
        }
    }
    
    private func configureCamera() {
        videoLayer = cameraService.previewLayer
        cameraService.captureHandler = handleCapture(with:)
        
        if cameraService.configure() {
            cameraService.start()
        }
    }
    
    private func handleCapture(with sampleBuffer: CMSampleBuffer?) {
        guard shouldMakePhoto, let sampleBuffer = sampleBuffer else {
            return
        }
        stopCamera()
        
        guard let originalImage = sampleBuffer.image() else {
            shouldMakePhoto = false
            cameraService.start()
            return
        }
        let compressedImageData = UIImage.compressedData(with: originalImage)
        
        sendImage(with: compressedImageData.base64EncodedString())
    }
    
    private func sendImage(with base64String: String) {
        let carImage = CarImage(content: base64String)
        
        firstly {
            apiService.recognizeCar(on: carImage)
        }.compactMap { response in
            self.extractPreferred(fromProbabilities: response.probabilities)
        }.compactMap { carInfo in
            self.extractCarBrandAndModel(fromCarInfo: carInfo)
        }.then { brandName, modelName in
            self.apiService.marketplace(ofBrandName: brandName, modelName: modelName).firstValue
        }.done { carModel in
            self.delegate?.scannerController(self, didFinishScanningWithCar: carModel)
        }.catch { error in
            let feedbackGenerator = UINotificationFeedbackGenerator()
            feedbackGenerator.prepare()
            feedbackGenerator.notificationOccurred(.error)
            self.startCamera()
        }
    }
    
    private func extractPreferred(fromProbabilities probabilities: [String: Double]) -> String? {
        guard let bestMatch = probabilities.max(by: { first, second -> Bool in
            first.value < second.value
        }) else {
            return nil
        }
        
        let carInfo = bestMatch.key
        return bestMatch.value > 0.35 ? carInfo : nil
    }
    
    private func extractCarBrandAndModel(fromCarInfo carInfo: String) -> (brandName: String, modelName: String)? {
        let carInfoComponents = carInfo.split(separator: " ")
        guard carInfoComponents.count > 1 else {
            return nil
        }
        
        let brandName = String(carInfoComponents[0])
        let modelName = carInfoComponents[1..<carInfoComponents.count].joined(separator: " ")
        
        return (brandName, modelName)
    }
}

extension ScannerViewController {
    func addPanModal(viewController: UIViewController & PanModalPresentable) {
        presentPanModal(viewController)
    }
}
