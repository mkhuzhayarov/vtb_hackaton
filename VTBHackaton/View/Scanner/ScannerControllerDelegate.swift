//
//  ScannerControllerDelegate.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 10.10.2020.
//

import Foundation

protocol ScannerControllerDelegate: AnyObject {
    func scannerController(_ scanner: ScannerViewController, didFinishScanningWithCar carModel: CarModel)
}
