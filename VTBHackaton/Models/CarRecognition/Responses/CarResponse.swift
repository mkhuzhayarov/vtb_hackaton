//
//  CarResponse.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import Foundation

struct CarResponse: Codable {
    let probabilities: [String: Double]
}
