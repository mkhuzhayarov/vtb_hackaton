//
//  Phone.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

struct Phone: Codable {
    let phone: String
}
