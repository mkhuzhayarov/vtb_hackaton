//
//  AuthResponse.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import Foundation

struct AuthResponse: Codable {
    let token: String
    let user: User
    let profile: Profile
}
