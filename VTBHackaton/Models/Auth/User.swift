//
//  User.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import Foundation

struct User: Codable {
    let id: Int
    let userName: String
    
    private enum CodingKeys: String, CodingKey {
        case id
        case userName = "username"
    }
}
