//
//  Profile.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import Foundation

struct Profile: Codable {
    let id: Int
    
    let birthDateTime: Date?
    let birthPlace: String?
    
    let familyName: String?
    let firstName: String?
    let middleName: String?
    
    let gender: Gender?
    
    let nationalityCode: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case birthDateTime = "birth_date_time"
        case birthPlace = "birth_place"
        case familyName = "family_name"
        case firstName = "first_name"
        case middleName = "middle_name"
        case gender
        case nationalityCode = "nationality_country_code"
    }
}
