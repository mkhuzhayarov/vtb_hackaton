//
//  PhoneCode.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

struct PhoneCode: Codable {
    let phone: String
    let code: Int
}
