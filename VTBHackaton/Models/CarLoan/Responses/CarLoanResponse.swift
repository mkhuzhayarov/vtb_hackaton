//
//  CarLoanResponse.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 08.10.2020.
//

import Foundation

struct CarLoanResponse: Codable {
    let application: Application
    let dateTime: Date
    
    struct Application: Codable {
        let clientId: Int
        let decision: Decision
        
        private enum CodingKeys: String, CodingKey {
            case clientId = "VTB_client_ID"
            case decision = "decision_report"
        }
    }
    
    struct Decision: Codable {
        let status: ApplicationStatus
        
        let comment: String
        
        let decisionDate: String // yyyy-MM-dd
        let decisionEndDate: String // yyyy-MM-dd
        
        let monthlyPayment: Double
        
        private enum CodingKeys: String, CodingKey {
            case status = "application_status"
            case comment
            case decisionDate = "decision_date"
            case decisionEndDate = "decision_end_date"
            case monthlyPayment = "monthly_payment"
        }
    }
    
    private enum CodingKeys: String, CodingKey {
        case application
        case dateTime = "datetime"
    }
}
