//
//  ApplicationStatus.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 08.10.2020.
//

import Foundation

enum ApplicationStatus: String, Codable {
    case prescoreApproved = "prescore_approved"
    case prescoreDenied = "prescore_denied"
    case processing = "processing"
}
