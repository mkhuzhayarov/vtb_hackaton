//
//  Person.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 08.10.2020.
//

import Foundation

struct Person: Codable {
    let birthDateTime: String
    let birthPlace: String
    
    let familyName: String
    let firstName: String
    let middleName: String
    
    let gender: Gender
    
    let nationalityCode: String
    
    enum CodingKeys: String, CodingKey {
        case birthDateTime = "birth_date_time"
        case birthPlace = "birth_place"
        case familyName = "family_name"
        case firstName = "first_name"
        case middleName = "middle_name"
        case gender
        case nationalityCode = "nationality_country_code"
    }
}
