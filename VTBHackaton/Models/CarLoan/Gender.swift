//
//  Gender.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 08.10.2020.
//

import Foundation

enum Gender: String, Codable, CaseIterable {
    case male = "male"
    case female = "female"
    case unknown = "unknown"
}
