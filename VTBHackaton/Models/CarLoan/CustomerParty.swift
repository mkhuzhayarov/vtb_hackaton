//
//  CustomerParty.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 08.10.2020.
//

import Foundation

struct CustomerParty: Codable {
    let person: Person
    
    let email: String
    let phone: String
    
    let incomeAmount: Int
    
    private enum CodingKeys: String, CodingKey {
        case person
        case email
        case phone
        case incomeAmount = "income_amount"
    }
}
