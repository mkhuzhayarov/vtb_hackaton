//
//  CarLoan.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 08.10.2020.
//

import Foundation

struct CarLoan: Codable {
    let dateTime: Date
    
    let interestRate: Double
    
    let requestedAmount: Int
    let requestedTerm: Int
    
    let tradeMark: String
    let vehicleCost: Int
    
    let customerParty: CustomerParty?
    let comment: String?
    
    private enum CodingKeys: String, CodingKey {
        case dateTime = "datetime"
        case interestRate = "interest_rate"
        case requestedAmount = "requested_amount"
        case requestedTerm = "requested_term"
        case tradeMark = "trade_mark"
        case vehicleCost = "vehicle_cost"
        case customerParty = "customer_party"
        case comment
    }
}
