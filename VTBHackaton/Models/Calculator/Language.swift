//
//  Language.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 09.10.2020.
//

import Foundation

enum Language: String, Codable {
    case ru = "ru-RU"
    case en = "en"
}
