//
//  CalculatorSettings.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import Foundation

struct CalculatorSettings: Codable {
    let name: String
    let language: Language
    
    let programs: [String]
    
    let clientTypes: [String]
    
    let specialConditions: [SpecialCondition]
    
    let variant: Variant
    
    let cost: Double
    let initialFee: Double
    
    let openInNewTab: Bool
    
    let kaskoDefaultValue: Double?
    
    struct Variant: Codable {
        let id: String
        let name: String
        let language: Language
    }
}
