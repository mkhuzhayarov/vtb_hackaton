//
//  Condition.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 10.10.2020.
//

import Foundation

enum Condition: String, Codable, CaseIterable {
    case kasko = "b907b476-5a26-4b25-b9c0-8091e9d5c65f"
    case insurance = "57ba0183-5988-4137-86a6-3d30a4ed8dc9"
    case fullDocuments = "cbfc4ef3-af70-4182-8cf6-e73f361d1e68"
}
