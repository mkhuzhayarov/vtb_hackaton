//
//  ScheduleRequest.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

struct ScheduleRequest: Codable {
    let contractRate: Double
    let lastPayment: Double
    let loanAmount: Double
    let payment: Double
    let term: Int
}
