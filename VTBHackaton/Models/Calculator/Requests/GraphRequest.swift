//
//  GraphRequest.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 10.10.2020.
//

struct GraphRequest: Codable {
    let contractRate: Double // Базовая процентная ставка, %
    let lastPayment: Double // Остаточный платёж, руб
    let loanAmount: Double // Сумма кредита, руб
    let payment: Double // Ежемесячный платёж, руб
    let term: Int // Срок кредита, лет
}
