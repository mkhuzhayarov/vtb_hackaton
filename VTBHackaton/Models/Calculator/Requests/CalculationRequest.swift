//
//  CalculationRequest.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import Foundation

struct CalculationRequest: Codable {
    let clientTypes: [String]
    
    let cost: Int
    let initialFee: Int
    
    let kaskoValue: Int
    
    let language: Language
    
    let residualPayment: Double
    
    let settingsName: String
    
    let specialConditions: [String]
    let term: Int
}

extension CalculationRequest {
    static func configuredMinimumFeeRequest(with cost: Int) -> CalculationRequest {
        return CalculationRequest(
            clientTypes: [],
            cost: cost,
            initialFee: Int(Double(cost) * 0.20),
            kaskoValue: Int(Double(cost) * 0.05),
            language: .ru,
            residualPayment: 0,
            settingsName: "Haval",
            specialConditions: [
                Condition.kasko.rawValue,
                Condition.insurance.rawValue,
                Condition.fullDocuments.rawValue
            ],
            term: 7
        )
    }
    
    static func configuredAverageFeeRequest(with cost: Int) -> CalculationRequest {
        return CalculationRequest(
            clientTypes: [],
            cost: cost,
            initialFee: Int(Double(cost) * 0.50),
            kaskoValue: Int(Double(cost) * 0.05),
            language: .ru,
            residualPayment: 0,
            settingsName: "Haval",
            specialConditions: [
                Condition.kasko.rawValue,
                Condition.insurance.rawValue,
                Condition.fullDocuments.rawValue
            ],
            term: 7
        )
    }
}

// MARK: - Business Logic
extension CalculationRequest {
    func minimumInitialFree() -> Int {
        Int(Double(cost) * 0.20)
    }
    
    func maximumInitialFee() -> Int {
        Int(Double(cost) * 0.80)
    }
    
    func minimumTerm() -> Int {
        1
    }
    
    func maximumTerm() -> Int {
        7
    }
}
