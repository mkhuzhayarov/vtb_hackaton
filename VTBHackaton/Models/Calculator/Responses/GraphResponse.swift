//
//  GraphResponse.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 10.10.2020.
//

struct GraphResponse: Codable {
    let payments: [Payment]
}
