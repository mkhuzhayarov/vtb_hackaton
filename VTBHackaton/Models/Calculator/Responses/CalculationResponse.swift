//
//  CalculationResponse.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import Foundation

struct CalculationResponse: Codable {
    let program: Program
    let result: Result
    let ranges: Ranges
    
    struct Program: Codable {
        let id: String
        let name: String
        
        let url: URL
        let requestUrl: URL
        
        let cost: CalculatorRange
        
        private enum CodingKeys: String, CodingKey {
            case id
            case name = "programName"
            case url = "programUrl"
            case requestUrl
            case cost
        }
    }
    
    struct Result: Codable {
        let payment: Int
        let term: Int
        let loanAmount: Int
        
        let residualPayment: Double?
        let subsidy: Int?
        let contractRate: Double
        
        let lastPayment: Double?
        
        let kaskoCost: Int?
    }
    
    struct Ranges: Codable {
        let cost: CalculatorRange
        let initialFee: CalculatorRange
        let residualPayment: CalculatorRange
        let term: CalculatorRange
    }
}
