//
//  CalculatorRange.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import Foundation

struct CalculatorRange: Codable {
    let min: Int?
    let max: Int?
    let isFilled: Bool
    
    private enum CodingKeys: String, CodingKey {
        case min
        case max
        case isFilled = "filled"
    }
}
