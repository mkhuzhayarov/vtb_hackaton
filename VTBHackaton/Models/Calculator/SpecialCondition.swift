//
//  SpecialCondition.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

struct SpecialCondition: Codable {
    let id: Condition
    let name: String
    let language: Language
    let excludingConditions: [String]
    let isChecked: Bool
}
