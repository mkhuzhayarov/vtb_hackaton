//
//  Payment.swift
//  VTBHackaton
//
//  Created by Dmitriy Zharov on 10.10.2020.
//

struct Payment: Codable {
    let balanceOut: Int // Остаток погашения
    let debt: Int // Основной долг
    let order: Int // Месяц платежа
    let payment: Int // Платеж в месяц
    let percent: Int // Оплата процентов
}
