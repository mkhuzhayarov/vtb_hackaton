//
//  CarBrand.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import Foundation

struct CarBrand: Codable {
    let absentee: Bool
    let alias: String
    let country: Country
    
    let currentCarCount: Int
    let currentModelsTotal: Int
    
    let generations: [String]
    
    let id: Int
    
    let title: String
    let titleRus: String
    
    let logoUrl: URL
    
    let models: [CarModel]
    
    let isOutbound: Bool
    
    private enum CodingKeys: String, CodingKey {
        case absentee
        case alias
        case country
        case currentCarCount
        case generations
        case currentModelsTotal
        case id
        case title
        case titleRus
        case logoUrl = "logo"
        case models
        case isOutbound
    }
}
