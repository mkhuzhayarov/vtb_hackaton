//
//  CarModel.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import Foundation

struct CarModel: Codable {
    let absentee: Bool
    
    let alias: String
    
    let bodies: [CarBody]
    
    let brand: CarBrandCompact
    
    let carId: String
    
    let colorsCount: Int
    let count: Int
    
    let hasSpecialPrice: Bool
    
    let id: Int
    
    let metallicPay: Int
    
    let minPrice: Int
    let model: CarModelCompact
    
    let ownTitle: String
    let pearlPay: Int
    
    let photoUrl: URL
    let prefix: String
    
    let premiumPriceSpecials: [String]
    let renderPhotos: [String: [String: CarRenderPhoto]]
    let sizesPhotos: [String: URL]
    
    let specmetallicPay: Int
    
    let title: String
    let titleRus: String
    let description: String?
    
    let transportType: TransportType
    
    private enum CodingKeys: String, CodingKey {
        case absentee
        case alias
        case bodies
        case brand
        case carId
        case colorsCount
        case count
        case hasSpecialPrice
        case id
        case metallicPay
        case minPrice
        case model
        case ownTitle
        case pearlPay
        case photoUrl = "photo"
        case prefix
        case premiumPriceSpecials
        case renderPhotos
        case sizesPhotos
        case specmetallicPay
        case title
        case titleRus
        case description
        case transportType
    }
}
