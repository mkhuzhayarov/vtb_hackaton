//
//  CarRenderType.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 10.10.2020.
//

enum CarRenderType: String, Codable {
    case renderWidgetLeft = "render_widget_left"
    case side
    case mainLeft = "main_left"
    case back
    case main
    case sideRight = "side_right"
    case renderWidgetRight = "render_widget_right"
    case front = "front"
    case renderSide = "render_side"
}
