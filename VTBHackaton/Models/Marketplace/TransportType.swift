//
//  TransportType.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import Foundation

struct TransportType: Codable {
    let alias: String
    let id: Int
    let title: String
}
