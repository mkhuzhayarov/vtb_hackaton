//
//  CarRenderPhoto.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import Foundation

struct CarRenderPhoto: Codable {
    let height: Double
    let width: Double
    
    let path: String
}
