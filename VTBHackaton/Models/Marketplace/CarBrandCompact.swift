//
//  CarBrandCompact.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import Foundation

struct CarBrandCompact: Codable {
    let absentee: Bool
    let alias: String
    let country: Country
    
    let id: Int
    
    let title: String
    let titleRus: String
    
    let logoUrl: URL
    
    let isOutbound: Bool
    
    private enum CodingKeys: String, CodingKey {
        case absentee
        case alias
        case country
        case id
        case title
        case titleRus
        case logoUrl = "logo"
        case isOutbound
    }
}
