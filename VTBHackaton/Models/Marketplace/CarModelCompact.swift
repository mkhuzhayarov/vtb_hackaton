//
//  CarModelCompact.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import Foundation

struct CarModelCompact: Codable {
    let absentee: Bool
    let alias: String
    
    let id: Int
    
    let prefix: String
    
    let title: String
    let titleRus: String
}
