//
//  Country.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import Foundation

struct Country: Codable {
    let code: String
    let id: Int
    let title: String
}
