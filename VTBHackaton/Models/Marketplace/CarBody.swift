//
//  CarBody.swift
//  VTBHackaton
//
//  Created by Александр Пономарев on 09.10.2020.
//

import Foundation

struct CarBody: Codable {
    let alias: String
    
    let doorsCount: Int
    
    let photoUrl: URL
    
    let title: String
    
    let type: String
    let typeTitle: String
    
    private enum CodingKeys: String, CodingKey {
        case alias
        case doorsCount = "doors"
        case photoUrl = "photo"
        case title
        case type
        case typeTitle
    }
}
